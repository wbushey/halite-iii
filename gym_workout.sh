#!/bin/sh

set -e

export REPLAYS_DIR="$(pwd)/../replays/gym"

hlt gym evaluate -b halite -i 100 --output-dir $REPLAYS_DIR
