#!/bin/sh

set -e

LOGLEVEL=debug
REPLAYS_DIR="$(pwd)/../replays/run_game"
./gradlew shadowJar
halite --replay-directory $REPLAYS_DIR  -vvv --width 64 --height 64 "java -Dlog.level=$LOGLEVEL -jar build/libs/MyBot.jar" "java -Dlog.level=$LOGLEVEL -jar build/libs/MyBot.jar" "java -Dlog.level=$LOGLEVEL -jar build/libs/MyBot.jar" "java -Dlog.level=$LOGLEVEL -jar build/libs/MyBot.jar"

. ./save_flog.sh
