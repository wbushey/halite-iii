package hlt

import bot.logging.KLogger
import bot.logging.configLogger
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.ThreadContext
import java.util.*


class Game {
    var turnNumber: Int = 0
    val myId: PlayerId
    val players: ArrayList<Player> = ArrayList()
    val me: Player
    val gameMap: GameMap
    private val log: KLogger

    init {
        val constantsString = Input.readLine()!!
        Constants.populateConstants(constantsString)

        val input = Input.readInput()
        val numPlayers = input.nextInt
        myId = PlayerId(input.nextInt)

        // Now that we have a botId, we can log
        configLogger(myId)
        log = KLogger(LogManager.getLogger())
        ThreadContext.put("botId", myId.id.toString())
        log.info { constantsString }

        for (i in 0 until numPlayers) {
            players.add(Player._generate())
        }
        me = players[myId.id]
        gameMap = GameMap._generate()
        Constants.MAP_WIDTH = gameMap.width
        Constants.MAP_HEIGHT = gameMap.height
    }

    fun ready(name: String) {
        System.out.println(name)
    }

    /**
     * Updates players and game map based on input from the game.
     *
     * First, players are updated with the new number of ships, dropoffs, and halite.
     * Then, the gamemap is updated with the locations of structures and ships.
     */
    fun updateFrame() {
        turnNumber = Input.readInput().nextInt
        log.info { "=============== TURN $turnNumber ================" }

        // Updating player's ship, dropoff, and halite
        for (i in 0 until players.size) {
            val input = Input.readInput()

            val currentPlayerId = PlayerId(input.nextInt)
            val numShips = input.nextInt
            val numDropoffs = input.nextInt
            val halite = input.nextInt

            // Update counts and positions of ships and dropoffs. Std is read to get positions
            players[currentPlayerId.id]._update(numShips, numDropoffs, halite)
        }

        // Clear ships from the map (all spaces marked safe) and update halite based on observed changes
        gameMap._update()

        // Mark every existing ship position as unsafe
        // Indicate shipyards and dropoffs on map
        gameMap.setMapAtTurnStart(players, myId)
    }

    fun endTurn(commands: Collection<Command>) {
        for (command in commands) {
            System.out.print(command)
            System.out.print(' ')
        }
        System.out.println()
    }
}


