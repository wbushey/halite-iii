package hlt

class GameMap(val width: Int, val height: Int,
              val cells: List<List<MapCell>> = List(height) { y ->
                  List(width) { x -> MapCell(Position(x, y), 0) }
              }) {

    // Currently a static total of all the Halite on the board
    var totalHalite = 0

    fun at(position: Position): MapCell? {
        return cells[position.y][position.x]
    }

    fun at(entity: Entity): MapCell? {
        return at(entity.position)
    }

    /**
     * Returns a set of the MapCells found in the provided X and Y Ranges
     */
    fun subSet(xRange: IntRange, yRange: IntRange): Set<MapCell> {
        val subSetCells = mutableSetOf<MapCell>()
        xRange.forEach { x ->
            yRange.forEach { y ->
                subSetCells.add(at(Position(x, y))!!)
            }
        }

        return subSetCells.toSet()
    }

    fun calculateDistance(source: Position, target: Position): Int {
        val dx = Math.abs(source.x - target.x)
        val dy = Math.abs(source.y - target.y)

        val toroidal_dx = Math.min(dx, width - dx)
        val toroidal_dy = Math.min(dy, height - dy)

        return toroidal_dx + toroidal_dy
    }

    /**
     * Clears all marks for ships occupying or willBeOccupying from the map
     */
    fun clearMarks() {
        for (y in 0 until height) {
            for (x in 0 until width) {
                cells[y][x].ship = null
                cells[y][x].futureShips.clear()
                cells[y][x].nearByEnemyShips = 0
            }
        }
    }


    /**
     * Sets initial marks and updates dropoffs for the turn
     */
    fun setMapAtTurnStart(players: List<Player>, currentPlayerId: PlayerId) {
        for (player in players) {
            for (ship in player.ships.values) {
                at(ship)?.markOccupiedBy(ship)

                if (player.id != currentPlayerId) {
                    at(ship)?.markWillBeOccupiedBy(ship)
                    at(ship.position.directionalOffset(Direction.EAST))?.markWillBeOccupiedBy(ship)
                    at(ship.position.directionalOffset(Direction.WEST))?.markWillBeOccupiedBy(ship)
                    at(ship.position.directionalOffset(Direction.NORTH))?.markWillBeOccupiedBy(ship)
                    at(ship.position.directionalOffset(Direction.SOUTH))?.markWillBeOccupiedBy(ship)

                    breadthFirstTraversal(ship.position, Constants.INSPIRATION_RADIUS) {
                        it.nearByEnemyShips++
                    }
                }
            }

            at(player.shipyard)?.structure = player.shipyard

            for (dropOff in player.dropoffs.values) {
                at(dropOff)?.structure = dropOff
            }
        }
    }

    /**
     * Perform a breadth first traversal of the GameMap for all cells around the provided Position within maxDistance.
     * `visit` is executed for every traversed cell.
     */
    fun breadthFirstTraversal(startingPosition: Position, maxDistance: Int, visit: (MapCell) -> Any) {
        val queue = mutableListOf<Position>()
        val visited = mutableSetOf<Position>()
        queue.add(startingPosition)

        while (queue.isNotEmpty()) {
            val currentPosition = queue.removeAt(0)
            if (!visited.contains(currentPosition)) {
                visit(this.at(currentPosition)!!)
                visited.add(currentPosition)
            }

            queue.addAll(
                    Direction.ALL_CARDINALS
                            .map { direction -> currentPosition.directionalOffset(direction) }
                            .filter { calculateDistance(it, startingPosition) <= maxDistance && !visited.contains(it) }
            )
        }
    }

    // Called at the beginning of each turn
    internal fun _update() {
        clearMarks()
        updateObservedHalite()
    }

    // Update halite values of positions with changed halite values based on Standard Input
    private fun updateObservedHalite() {
        val updateCount = Input.readInput().nextInt

        for (i in 0 until updateCount) {
            val input = Input.readInput()
            val x = input.nextInt
            val y = input.nextInt

            totalHalite -= cells[y][x].halite
            val newHaliteValue = input.nextInt
            cells[y][x].halite = newHaliteValue
            totalHalite += newHaliteValue
        }
    }

    companion object {

        /**
         * Read in Halite values from Std and build a new GameMap
         */
        internal fun _generate(): GameMap {
            val mapInput = Input.readInput()
            val width = mapInput.nextInt
            val height = mapInput.nextInt

            val map = GameMap(width, height)

            for (y in 0 until height) {
                val rowInput = Input.readInput()

                for (x in 0 until width) {
                    val halite = rowInput.nextInt
                    map.totalHalite += halite
                    map.cells[y][x].halite = halite
                }
            }

            return map
        }
    }
}