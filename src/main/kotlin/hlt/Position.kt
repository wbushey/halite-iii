package hlt

import org.apache.logging.log4j.ThreadContext

/**
 * Output a log message with a temporarily set Position in the ThreadContext
 */
fun withTemporaryContextPosition(position: Position, block: ()-> Any): Any{
    val originalTCX = ThreadContext.get("position-x")
    val originalTCY = ThreadContext.get("position-y")
    ThreadContext.put("position-x", position.x.toString())
    ThreadContext.put("position-y", position.y.toString())
    val retval = block.invoke()

    if (originalTCX != null && originalTCY != null){
        ThreadContext.put("position-x", originalTCX)
        ThreadContext.put("position-y", originalTCY)
    } else {
        ThreadContext.removeAll(listOf("position-x", "position-y"))
    }

    return retval
}

class Position{
    val x: Int
    val y: Int

    constructor(_X: Int, _Y: Int) {
        x = (_X % Constants.MAP_WIDTH + Constants.MAP_WIDTH) % Constants.MAP_WIDTH
        y = (_Y % Constants.MAP_HEIGHT + Constants.MAP_HEIGHT) % Constants.MAP_HEIGHT
    }

    internal fun directionalOffset(d: Direction): Position {
        val dx: Int
        val dy: Int

        when (d) {
            Direction.NORTH -> {
                dx = 0
                dy = -1
            }
            Direction.SOUTH -> {
                dx = 0
                dy = 1
            }
            Direction.EAST -> {
                dx = 1
                dy = 0
            }
            Direction.WEST -> {
                dx = -1
                dy = 0
            }
            Direction.STILL -> {
                dx = 0
                dy = 0
            }
        }

        return Position(x + dx, y + dy)
    }

    // Bitwise shift X 7 bits, then add Y
    override fun hashCode(): Int = (x shl 7) + y

    override fun equals(other: Any?): Boolean {
        return (other is Position && other.hashCode() == hashCode())
    }

    override fun toString() = "Position($x, $y)"
}
