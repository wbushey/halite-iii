package hlt

sealed class Command {

    companion object {
        fun spawnShip(): Command = Spawn
        fun transformShipIntoDropoffSite(id: EntityId) = TransformToDropoff(id)
        fun move(id: EntityId, direction: Direction) = Move(id, direction)
    }

    /**
     * Command to create a new ship
     */
    object Spawn: Command() {
        override fun toString() = "g"
    }

    /**
     * Command to turn a ship into a dropoff
     */
    data class TransformToDropoff(val shipId: EntityId): Command(){
        override fun toString() = "c ${shipId.id}"
    }

    /**
     * Command to move a ship
     */
    data class Move(val shipId: EntityId, val direction: Direction): Command(){
        override fun toString() = "m ${shipId.id} ${direction.charValue}"
    }

}
