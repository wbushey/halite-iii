package hlt

import bot.logging.KLogger
import org.apache.logging.log4j.LogManager

private val log : KLogger = KLogger(LogManager.getLogger())

class MapCell(val position: Position, var halite: Int) {
    var ship: Ship? = null
    var nearestDropoffPosition: Position? = null
    var nearestDropoffDistance: Int? = null
    var nearByEnemyShips: Int = 0
    val futureShips = mutableListOf<Ship>()
    var structure: Entity? = null

    val isEmpty: Boolean
        get() = ship == null && structure == null

    val isOccupied: Boolean
        get() = ship != null

    val willBeOccupied: Boolean
        get() = futureShips.isNotEmpty()

    val willNotBeOccupied: Boolean
        get() = !willBeOccupied

    val hasStructure: Boolean
        get() = structure != null

    fun markOccupiedBy(ship: Ship) {
        this.ship = ship
    }

    fun markWillBeOccupiedBy(ship: Ship){
        withTemporaryContextPosition(position){
            log.debug{"Will be occupied by Ship: ${ship.id} of ${ship.owner}"}
        }
        futureShips.add(ship)
    }
}
