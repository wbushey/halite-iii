package hlt

const val MAX_BUFFER = 10

class Ship(owner: PlayerId, id: EntityId, position: Position, val halite: Int) : Entity(owner, id, position) {

    val isFull: Boolean
        get() = halite >= Constants.MAX_HALITE - MAX_BUFFER

    internal fun makeDropoff(): Command {
        return Command.transformShipIntoDropoffSite(id)
    }

    // Make the present space as Unsafe for other ships, and stay still
    internal fun stayStill(map: GameMap): Command.Move {
        return moveTo(Direction.STILL, map)
    }

    // Mark the space the Ship is moving into as Unsafe for other ships, then move
    internal fun moveTo(direction: Direction, map: GameMap): Command.Move {
        map.at(position.directionalOffset(direction))?.markWillBeOccupiedBy(this)
        return Command.move(id, direction)
    }

    companion object {

        internal fun _generate(playerId: PlayerId): Ship {
            val input = Input.readInput()

            val shipId = EntityId(input.nextInt)
            val x = input.nextInt
            val y = input.nextInt
            val halite = input.nextInt

            return Ship(playerId, shipId, Position(x, y), halite)
        }
    }
}

