package hlt

enum class Direction constructor(val charValue: Char) {
    NORTH('n'),
    EAST('e'),
    SOUTH('s'),
    WEST('w'),
    STILL('o');

    fun invertDirection(): Direction {
        when (this) {
            NORTH -> return SOUTH
            EAST -> return WEST
            SOUTH -> return NORTH
            WEST -> return EAST
            STILL -> return STILL
            else -> throw IllegalStateException("Unknown direction " + this)
        }
    }

    companion object {
        val ALL_CARDINALS = listOf(NORTH, SOUTH, EAST, WEST)
        val ALL_DIRECTIONS = ALL_CARDINALS + Direction.STILL
    }
}
