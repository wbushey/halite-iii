@file:Suppress("WildcardImport", "MagicNumber")

package bot

import bot.assets.FleetStatus
import bot.executives.Commander
import bot.logging.KLogger
import hlt.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.ThreadContext


fun main(args: Array<String>) {
    // game must be initialized before calling getLogger(), so botId system property can be set
    val game = Game()
    val log = KLogger(LogManager.getLogger())

    log.info{"Initializing bot for ${game.myId}"}
    var me = game.me
    val fleetStatus = FleetStatus(game.gameMap, me.shipyard, me.ships, me.dropoffs, game.turnNumber)
    val fleetCommander = Commander(fleetStatus)

    log.info{"Successfully created bot!"}

    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    game.ready("MyKotlinBot")
    while (true) {
        game.updateFrame()
        ThreadContext.put("turnNumber", game.turnNumber.toString())

        log.info{"Updating fleet, gameStatus, and the commander data"}
        fleetStatus.apply {
            halite = me.halite
            turnNumber = game.turnNumber
        }
        fleetCommander.update()

        val commands = fleetCommander.commands

        commands.forEach {
            log.info{"$it"}
        }

        game.endTurn(commands)
    }
}


