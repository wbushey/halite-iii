package bot.executives

import bot.logging.KLogger
import bot.MutableTypeMap
import bot.analysis.BestPositions
import bot.analysis.BoundedCluster
import bot.analysis.MapClusterer
import bot.assets.FleetStatus
import bot.planning.Mission
import hlt.Command
import hlt.Constants
import hlt.Direction
import hlt.Position
import org.apache.logging.log4j.LogManager
import kotlin.math.*
import kotlin.random.Random

// Multiplier that determines when to stop building ships
const val BUILD_SHIPS_PERCENT_TURN_LIMIT = .5
// Multiplier that determines when to start building dropoffs
const val BUILD_DROPOFFS_PERCENT_TURN_FLOOR = .3
// How much Halite to save per turn for Dropoff building
const val MIN_DROPOFF_DISTANCE = 18
const val GATHER_AREA_GROWTH_END = 300
const val RUSHTODROPOFF_BUFFER_DIVISOR = 7
const val END_GAME_LENGTH = 75

private val log: KLogger = KLogger(LogManager.getLogger())

typealias CaptainsMap = MutableTypeMap<Captain>

/**
 * A Commander of a fleetStatus, given a strategy, sets goals for Captains over the course of the game. A Commander also
 * relays commands for the fleetStatus back to the game.
 */
class Commander(private val fleetStatus: FleetStatus,
                val captains: CaptainsMap = mutableMapOf(),
                private val rng: Random = Random(System.nanoTime())) {
    private val captainsFactory = Captain.Factory(fleetStatus)

    private val maxGatherDistance: Int
        get() = (1 + (fleetStatus.map.width.toDouble() / 2).pow(
                fleetStatus.turnNumber.toDouble() / GATHER_AREA_GROWTH_END)
                ).roundToInt()

    /**
     * Update the status of the Commander, of the Captains, and make mission changes if needed
     */
    fun update() {
        removeCaptainsWithMissingShips(captains, fleetStatus)
        createCaptainsForNewShips(captains, fleetStatus, captainsFactory)

        val remainingTurns = Constants.MAX_TURNS - fleetStatus.turnNumber
        val rushToDropoffBuffer = getRushToDropoffBuffer(captains, fleetStatus, remainingTurns)
        var numberOfDropoffsToBuild = numberOfDropoffsToBuild(fleetStatus)

        // Get interesting clusters if there are dropoffs to build
        val mapClusters = if (numberOfDropoffsToBuild > 0) {
            MapClusterer(fleetStatus.map, fleetStatus.averageHalitePerCell + 50).clusters
        } else {
            emptyList()
        }

        // Get positions of all our dropoffs/shipyard
        val currentDropoffPositions = fleetStatus.allCurrentDropoffPositions().toMutableSet()

        // Update all captains and make any mission changes
        captains.values
                .sortedByDescending { fleetStatus.map.calculateDistance(it.ship.position, fleetStatus.shipyard.position) }
                .forEach { captain ->
                    captain.update()

                    // Tell captains to go back as the game nears end
                    if (captain.shouldRushToDropoff(remainingTurns, rushToDropoffBuffer)) {
                        captain.mission = Mission.RushToDropoff()
                    }
                    // Tell captains to build Dropoffs when appropriate
                    else if (captain.shouldBuildDropoff(fleetStatus, numberOfDropoffsToBuild, currentDropoffPositions)) {
                        // Tell the captain to build a dropoff if they are within an interesting cluster
                        if (captain.assignDropoffMission(mapClusters, currentDropoffPositions)) {
                            // Reduce the number of dropoffs to build, so we don't overbuild Dropoffs
                            numberOfDropoffsToBuild--
                        }
                    }
                }

        // Assign missions to captains in need of them
        assignMissions()
    }

    val commands: List<Command>
        get() {
            @Suppress("MaxLineLength")
            log.info { "Commander's Status | fleetStatus halite: ${fleetStatus.halite} | turn number: ${fleetStatus.turnNumber}" }

            val commandQueue = ArrayList<Command>()

            // Add commands from all captains
            val sortedCaptains = captains.prioritized()
            @Suppress("MaxLineLength")
            log.debug { "Ship order: ${sortedCaptains.map { "${it.ship.id}: ${it.possibleDirections.count()}" }.joinToString { "," }}" }
            commandQueue.addAll(sortedCaptains.map { it.chooseCommand() })

            // Build ships early in the game
            if (shouldBuildShip(fleetStatus)) {
                commandQueue.add(fleetStatus.shipyard.spawn())
            }

            return commandQueue
        }

    // Assign halite collection missions to captains in need of them
    private fun assignCollectMissions(missionlessCaptains: Collection<Captain>) {
        if (missionlessCaptains.isEmpty())
            return

        // Get the MapCells that are currently the target of missions
        val targetedPositions = captains.values
                .filter { it.mission is Mission.Gather }
                .map { (it.mission as Mission.Gather).target }

        // Find the best positions
        val bestPositions = BestPositions(
                missionlessCaptains.count(),
                fleetStatus.shipyard.position.directionalOffset(Direction.NORTH),
                Comparator { a, b -> fleetStatus.expectedRoundTripValue(a) - fleetStatus.expectedRoundTripValue(b) }
        )

        val cellsOfFocus = fleetStatus.map.subSet(
                (fleetStatus.shipyard.position.x - maxGatherDistance)..(fleetStatus.shipyard.position.x + maxGatherDistance),
                (fleetStatus.shipyard.position.y - maxGatherDistance)..(fleetStatus.shipyard.position.y + maxGatherDistance)
        )
        cellsOfFocus.forEach { cell ->
            if (!targetedPositions.contains(cell.position))
                bestPositions.conditionallyInsert(cell.position)
        }

        // Assign captains to gather from the best cells
        missionlessCaptains.forEachIndexed { index, captain ->
            captain.mission = Mission.Gather(bestPositions.cells.get(index))
        }
    }

    // Assign missions to captains in need of them
    private fun assignMissions() {
        // Get the captains in need of missions
        val missionlessCaptains = captains.values.filter { it.mission.isComplete }

        if (missionlessCaptains.isEmpty())
            return

        // Assign collection missions to remaining missionless captains
        assignCollectMissions(missionlessCaptains)
    }
}

// Should the commander build a ship?
internal fun shouldBuildShip(fleetStatus: FleetStatus): Boolean {
    return fleetStatus.turnNumber <= (Constants.MAX_TURNS * BUILD_SHIPS_PERCENT_TURN_LIMIT)
            && fleetStatus.halite >= Constants.SHIP_COST
            && fleetStatus.map.at(fleetStatus.shipyard)!!.willNotBeOccupied
}

// If we could, how many dropoffs should we build this turn?
internal fun numberOfDropoffsToBuild(fleetStatus: FleetStatus): Int {
    val workingHalite = if (fleetStatus.turnNumber <= (Constants.MAX_TURNS * BUILD_SHIPS_PERCENT_TURN_LIMIT))
        fleetStatus.halite - Constants.SHIP_COST
    else
        fleetStatus.halite
    return if (workingHalite > (Constants.MAX_TURNS * BUILD_DROPOFFS_PERCENT_TURN_FLOOR)) {
        min(workingHalite / Constants.DROPOFF_COST, fleetStatus.targetNumberOfDropoffs - (fleetStatus.dropoffs.count()))
    } else {
        0
    }

}

/* ******************************************Captain Extention Functions****************************************** */
/*
 * Get a list of the captains, prioritized by the number of moves they can make
 */
internal fun CaptainsMap.prioritized(): List<Captain> {
    return this.values.toList().sortedWith(compareBy({ it.possibleDirections.count() }, { it.ship.id.id }))
}

/*
 * Indicates if a captain should be directed to rush to a dropoff
 */
private fun Captain.shouldRushToDropoff(remainingTurns: Int, rushToDropoffBuffer: Double): Boolean {
    return remainingTurns <= END_GAME_LENGTH
            && this.mission !is Mission.RushToDropoff
            && abs(this.distanceToNearestDropoff - remainingTurns) <= rushToDropoffBuffer
}

/*
 * Indicates if the captain should be directed to build a dropoff
 */
private fun Captain.shouldBuildDropoff(fleetStatus: FleetStatus, numberOfDropoffsToBuild: Int,
                                       currentDropoffPositions: MutableCollection<Position>): Boolean {
    val minDropoffDistance = MIN_DROPOFF_DISTANCE + (Constants.MAP_HEIGHT - 32) / 8
    return numberOfDropoffsToBuild > 0
            && !(fleetStatus.map.at(this.ship.position)!!.hasStructure)
            && currentDropoffPositions
            .all { dropoffPosition -> fleetStatus.map.calculateDistance(this.ship.position, dropoffPosition) >= minDropoffDistance }
}

/*
 * If possible, give the captain a mission to build a Dropoff.
 *
 * Returns true if possible, and adds position of new Dropoff to currentDropoffPositions. Returns false otherwise.
 */
private fun Captain.assignDropoffMission(mapClusters: List<BoundedCluster>,
                                         currentDropoffPositions: MutableCollection<Position>): Boolean {
    // Tell the captain to build a dropoff if they are within an interesting cluster
    return if (mapClusters.any {
                this.ship.position.x >= it.minX && this.ship.position.x <= it.maxX
                        && this.ship.position.y >= it.minY && this.ship.position.y <= it.maxY
            }) {

        // Give the captain a mission to build a Dropoff
        this.mission = Mission.CreateDropoff

        // Add this position to list of current Dropoff Positions
        currentDropoffPositions.add(this.ship.position)

        true
    } else {
        false
    }
}

/* ******************************Captain Removal and Addition Functions***************************************** */
private fun removeCaptainsWithMissingShips(captains: CaptainsMap, fleetStatus: FleetStatus) {
    captains.keys.minus(fleetStatus.ships.keys).forEach {
        log.warn { "Removing Captain ${it.id}" }
        captains.remove(it)
    }
}

private fun createCaptainsForNewShips(captains: CaptainsMap, fleetStatus: FleetStatus, captainsFactory: Captain.Factory) {
    fleetStatus.ships.keys.minus(captains.keys).forEach {
        captains[it] = captainsFactory.build(it)
    }
}


/*
 * Number of turns to allow for ships to return to dropoffs, in adddition to travel time.
 * This accounts for time the fact that a dropoff can only accept four ships at a time
 */
private fun getRushToDropoffBuffer(captains: CaptainsMap, fleetStatus: FleetStatus, remainingTurns: Int): Double {
    return (captains.count() / 4 * fleetStatus.dropoffs.count()) + ceil(remainingTurns.toDouble() / RUSHTODROPOFF_BUFFER_DIVISOR)
}


