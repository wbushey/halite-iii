package bot.executives.decisions

import bot.planning.Mission
import bot.planning.Step
import hlt.*

const val STANDSTILL_THRESHOLD = 2

/**
 * Get the directions that a ship can move in given it's mission and nearest dropoff position
 */
internal fun GameMap.possibleDirections(
        ship: Ship,
        mission: Mission,
        nearestDropoffPosition: Position,
        turnsInCurrentPosition: Int
): List<Direction> {
    // If ship doesn't have enough halite to move, only possibility is staying still
    if (ship.halite < this.at(ship.position)!!.halite / Constants.MOVE_COST_RATIO)
        return listOf(Direction.STILL)

    // If current mission is RushToDropoff and we're next to a Dropoff, don't consider anything else but
    // moving into the dropoff
    val distanceToNearestDropoff = this.calculateDistance(ship.position, nearestDropoffPosition)
    if (mission is Mission.RushToDropoff && distanceToNearestDropoff == 1)
        return listOf(Direction.ALL_CARDINALS.first { ship.position.directionalOffset(it) == nearestDropoffPosition })

    val possibleMoves = Direction.ALL_CARDINALS.toMutableList()

    // Only consider staying still possible if the ship is not on a dropoff/shipyard
    if (distanceToNearestDropoff > 0)
        possibleMoves.add(0, Direction.STILL)

    // Be willing to move into a dropoff or it's surroundings if an enemy ship is sitting on the dropoff
    if (distanceToNearestDropoff <= 2
            && mission.currentStep is Step.GoToDropoff
            && this.at(nearestDropoffPosition)!!.ship?.owner != ship.owner) {
        possibleMoves.removeConfirmedUnsafeMoves(this, ship)
    }
    // Different levels of safety depending on how long we've been in our current position
    else if (turnsInCurrentPosition <= STANDSTILL_THRESHOLD) {
        possibleMoves.removeAllPossibleUnsafeMoves(this, ship)
    }
    else {
        possibleMoves.removeConfirmedUnsafeMoves(this, ship)
    }

    return possibleMoves.toList()
}

// Remove directions into positions that might contain any ship
private fun MutableList<Direction>.removeAllPossibleUnsafeMoves(map: GameMap, ship: Ship) {
    removeAll { map.at(ship.position.directionalOffset(it))!!.willBeOccupied }
}

// Remove only directions into positions that definitely will have a friendly ship
private fun MutableList<Direction>.removeConfirmedUnsafeMoves(map: GameMap, ship: Ship) {
    removeAll { direction ->
        map.at(ship.position.directionalOffset(direction))!!
                .futureShips.any { otherShip -> otherShip.owner == ship.owner }
    }
}
