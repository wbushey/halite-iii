package bot.executives

import bot.logging.KLogger
import bot.assets.FleetStatus
import bot.executives.decisions.possibleDirections
import bot.planning.Mission
import bot.planning.Step
import hlt.*
import org.apache.logging.log4j.LogManager
import kotlin.random.Random


private val log: KLogger = KLogger(LogManager.getLogger())

/**
 * Given a mission, a Captain issues commands to achieve that mission. Captains also provide information about the
 * current status of their ship.
 */
class Captain private constructor(private val fleetStatus: FleetStatus,
                                  private val shipId: EntityId,
                                  private val rng: Random,
                                  var mission: Mission) {

    /**
     * Factory for creating Captains using a common FleetStatus
     */
    class Factory(private val fleetStatus: FleetStatus) {
        fun build(shipId: EntityId, initialMission: Mission = Mission.NoMission,
                  rng: Random = Random(System.nanoTime())): Captain {
            return Captain(fleetStatus, shipId, rng, initialMission)
        }
    }

    private var numberOfTurnsInCurrentPosition = 0
    private var previousPosition : Position? = null

    /**
     * The Captain's Ship
     */
    val ship: Ship
        get () = fleetStatus.ships.getValue(shipId)

    /**
     * The position of the nearest location that halite can be dropped off at
     */
    var nearestDropoffPosition: Position = fleetStatus.nearestDropoffPosition(ship.position)

    /**
     * The distance to the nearest location that halite can be dropped off at.
     */
    val distanceToNearestDropoff: Int
        get () = fleetStatus.map.calculateDistance(ship.position, nearestDropoffPosition)

    /**
     * Get the moves that this captain can make given it's position and it's mission
     */
    val possibleDirections: List<Direction>
        get () = fleetStatus.map.possibleDirections(ship, mission, nearestDropoffPosition, numberOfTurnsInCurrentPosition)

    /**
     * Update the status of the Captain
     */
    fun update() {
        // Find the nearest dropoff
        nearestDropoffPosition = fleetStatus.nearestDropoffPosition(ship.position)

        // Update the mission if possible and necessary
        if (!mission.isComplete && mission.currentStep.isComplete(this, fleetStatus)) {
            mission.advance()
            previousPosition = null
        }

        // Update the count of turns that we've been at the current position
        if (previousPosition == ship.position)
            numberOfTurnsInCurrentPosition++
        else
            numberOfTurnsInCurrentPosition = 0
    }

    /**
     * Decide what to do next
     */
    fun chooseCommand(): Command {
        previousPosition = ship.position
        // Set ThreadContext to support F-Log
        return withTemporaryContextPosition(ship.position) {
            // Local assignment because mission.currentStep is a complex expression
            val currentStep = mission.currentStep
            log.debug { "Mission: $mission | Step: $currentStep" }
            when (currentStep) {
                is Step.GoToCollect -> collectOrNavigateTo(currentStep)
                is Step.GoToDropoff -> navigateToAndRefill(currentStep)
                is Step.GoTo -> navigateTo(currentStep.target)
                is Step.Collect -> collect(currentStep)
                is Step.ConvertToDropoff -> Command.transformShipIntoDropoffSite(shipId)
                is Step.DelayedDropoff -> {
                    log.debug { "Planning Halite Dropoff" }
                    val newStep = currentStep.update(mission) { this.nearestDropoffPosition }
                    navigateTo(newStep.target)
                }
                else -> {
                    log.debug { "Don't know what to do. Staying Still." }
                    ship.stayStill(fleetStatus.map)
                }
            }
        } as Command
    }

    /**
     * The current cost to move away from the current Position
     */
    val costToMoveAway: Int
        get () = fleetStatus.costToMoveFrom(ship.position)

    /**
     * Amount of Halite that will be minded from the current Position if the ship stays still this turn
     */
    val expectedMinedHalite: Int
        get () = fleetStatus.expectedHaliteThisTurn(ship.position)

    internal fun collectOrNavigateTo(currentStep: Step.GoToCollect): Command{
        return if (fleetStatus.expectedHaliteThisTurn(ship.position) >= fleetStatus.averageCostToMoveFrom){
            navigateTo(ship.position)

        } else {
            navigateTo(currentStep.target)
        }
    }

    /**
     * Navigate to a position, and refill if appropirate on the way
     */
    internal fun navigateToAndRefill(currentStep: Step.GoToDropoff): Command{
        return if (fleetStatus.expectedHaliteThisTurn(ship.position) >= fleetStatus.averageCostToMoveFrom
                    && ship.halite < Constants.MAX_HALITE - fleetStatus.expectedHaliteThisTurn(ship.position)){
            navigateTo(ship.position)

        } else {
            navigateTo(currentStep.target)
        }
    }

    // Returns the next safe move that will get closer to the target position
    internal fun navigateTo(target: Position): Command {
        return ship.moveTo(
                possibleDirections
                 .shuffled(rng)
                 .ifEmpty{listOf(Direction.STILL)}
                 .minBy{
                     fleetStatus.map.calculateDistance(ship.position.directionalOffset(it), target)
                 }!!,
                fleetStatus.map)
    }

    // Returns an action to either collect Halite or move to an adjacent spot with more Halite
    internal fun collect(currentStep: Step.Collect): Command {
        // Stay at a local max until mining yields less halite than the average cost to move
        if (currentStep.atLocalMax){
            if (fleetStatus.expectedHaliteThisTurn(ship.position) > fleetStatus.averageCostToMoveFrom
                    && possibleDirections.contains(Direction.STILL))
                return ship.stayStill(fleetStatus.map)
            else
                currentStep.atLocalMax = false
        }

        val immediateHalite = possibleDirections.ifEmpty{listOf(Direction.STILL)}.map {
            it to fleetStatus.expectedHaliteThisTurn(ship.position.directionalOffset(it))
        }

        log.debug { "Possible directions: ${immediateHalite.joinToString(", ")}" }

        val uphill = immediateHalite.maxBy { it -> it.second }!!.first

        if (uphill == Direction.STILL)
            currentStep.atLocalMax = true

        return ship.moveTo(uphill, fleetStatus.map)
    }
}
