package bot

import hlt.*

typealias MutableTypeMap<T> = MutableMap<EntityId, T>
typealias ShipMap = MutableTypeMap<Ship>
typealias DropoffMap = MutableTypeMap<DropOff>
