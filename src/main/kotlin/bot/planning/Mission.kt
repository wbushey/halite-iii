package bot.planning

import hlt.Position

/**
 * Missions represent a primary goal for a Captain, include a list of Steps to execute the Mission, and indicate when
 * the Mission is complete.
 */
sealed class Mission(private val steps: MutableList<Step>) {
    private var currentIndex : Int = 0

    /**
     * The current Step in the mission
     */
    val currentStep: Step
        get() = steps[currentIndex]


    /**
     * Indicates that the mission is complete
     */
    val isComplete: Boolean
        get() = currentIndex >= steps.size

    /**
     * Advance the Mission to it's next Step.
     */
    fun advance() = currentIndex++

    /**
     * Replace the Mission's current Step with a new Step.
     */
    fun replaceCurrentStep(newStep: Step) {
        steps[currentIndex] = newStep
    }

    /**
     * A Mission to collection Halite from a location and take it to the Dropoff nearest to that location.
     */
    class Gather(val target: Position) : Mission(mutableListOf(Step.GoToCollect(target), Step.Collect(), Step.DelayedDropoff)){
        override fun toString() = "Gather at ${target.x}, ${target.y}"
    }

    /**
     * A Mission to rush to the nearest Dropoff
     */
    class RushToDropoff: Mission(mutableListOf(Step.DelayedDropoff)){
        override fun toString(): String = "RushToDropoff"
    }

    /**
     * A Mission to create a Dropoff at a location
     */
    object CreateDropoff: Mission(mutableListOf(Step.ConvertToDropoff)){
        override fun toString() = "CreateDropoff"
    }

    /**
     * A Mission to do nothing.
     */
    class DoNothing: Mission(mutableListOf(Step.Sit)){
        override fun toString() = "DoNothing"
    }

    /**
     * A Mission that is the lack of a mission
     */
    object NoMission: Mission(mutableListOf()){
        override fun toString() = "No Mission"
    }
}
