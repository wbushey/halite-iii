package bot.planning

import bot.assets.FleetStatus
import bot.executives.Captain
import hlt.Constants
import hlt.Position

const val EXPECTED_HALITE_THRESHOLD = 5

/**
 * Steps represent the actions that a Captain should currently perform, and indicate when the action is complete.
 */
sealed class Step {
    /**
     * Indicates whether the step has been completed or not.
     */
    open fun isComplete(captain: Captain, fleetStatus: FleetStatus): Boolean = false

    /**
     * GoTo represents a goal to travel to a target Position.
     */
    open class GoTo(var target: Position) : Step() {

        /**
         * Returns True when the Captain has moved her Ship to the target Position.
         */
        override fun isComplete(captain: Captain, fleetStatus: FleetStatus): Boolean = captain.ship.position == target

        override fun toString() = "GoTo ${target.x}, ${target.y}"

        override fun hashCode(): Int = target.hashCode()
        override fun equals(other: Any?): Boolean = if (other is GoTo) other.hashCode() == hashCode() else false
    }

    /**
     * Represents a goal to travel to a target Dropoff
     */
    class GoToDropoff(targetDropoff: Position) : GoTo(targetDropoff) {
        override fun toString() = "GoToDropoff ${target.x}, ${target.y}"
    }

    class GoToCollect(collectionTarget: Position): GoTo(collectionTarget){
        override fun toString() = "GoToCollect ${target.x}, ${target.y}"

        /**
         * Returns true if the ship is full, or if the GoTo destination has been reached
         */
        override fun isComplete(captain: Captain, fleetStatus: FleetStatus): Boolean{
            return captain.ship.isFull || super.isComplete(captain, fleetStatus)
        }
    }

    /**
     * Collect represents a goal of collecting Halite in the Position that a Ship is currently in.
     */
    data class Collect(var atLocalMax: Boolean = false) : Step() {

        /**
         * Returns True when the Ship has collected enough Halite, or if game time is running out.
         */
        override fun isComplete(captain: Captain, fleetStatus: FleetStatus): Boolean {
            return captain.ship.isFull
                    || (fleetStatus.map.at(captain.ship)!!.halite == 0 )
                    || captain.expectedMinedHalite < EXPECTED_HALITE_THRESHOLD
                    && captain.ship.halite > captain.costToMoveAway
        }

        override fun toString() = "Collect | AtLocalMax: $atLocalMax"
    }

    /**
     * Sit represents a goal of not moving.
     */
    object Sit : Step() {
        override fun toString() = "Sit"
    }

    /**
     * DelayedDropoff represents a Step for dropping off Halite whose planning is delayed.
     *
     * When this Step is reached, `update` can be called to replace this Step with a relevant GoTo Step.
     */
    object DelayedDropoff : Step() {

        /**
         * Replace this Step with a relevant GoTo Step in the mission. Also returns the relevant GoTo Step.
         *
         * @param mission: The Mission to update the current Step of. Should be the Mission that contains this Step.
         * @param getPosition: Function that will provide the Position that will become the target of the new GoTo Step.
         */
        fun update(mission: Mission, getPosition: () -> Position): Step.GoToDropoff {
            val newStep = GoToDropoff(getPosition())
            mission.replaceCurrentStep(newStep)
            return newStep
        }

        override fun toString() = "DelayedDropoff"
    }

    /**
     * Convert the ship into a dropoff at the current position
     */
    object ConvertToDropoff : Step() {
        /**
         * Returns True when the current position of the captain is a dropoff
         */
        override fun isComplete(captain: Captain, fleetStatus: FleetStatus): Boolean {
            return fleetStatus.dropoffs.any {
                it.value.position == captain.ship.position
            }
        }

        override fun toString() = "ConvertToDropoff"
    }
}
