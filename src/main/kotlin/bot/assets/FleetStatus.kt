package bot.assets

import bot.DropoffMap
import bot.ShipMap
import hlt.Constants
import hlt.GameMap
import hlt.Position
import hlt.Shipyard
import kotlin.math.ceil
import kotlin.math.max

const val TARGET_DROPOFF_DIVISOR = 1024
const val MINIMUM_DROPOFFS = 0
const val INSPIRATION_CONSIDERATION_BONUS = 10

/**
 * All of the physical assets of the fleet.
 */
data class FleetStatus(val map: GameMap,
                       val shipyard: Shipyard,
                       val ships: ShipMap,
                       val dropoffs: DropoffMap,
                       var halite: Int = 0,
                       var turnNumber: Int = 0) {

    init {
        setNearestDropoffs()
    }

    val averageHalitePerCell: Int
        get() = map.totalHalite / (map.width * map.height)

    val averageCostToMoveFrom: Int
        get() = averageHalitePerCell / Constants.MOVE_COST_RATIO

    val targetNumberOfDropoffs: Int = MINIMUM_DROPOFFS + (map.width * map.height / TARGET_DROPOFF_DIVISOR)

    /**
     * Return the cost for a Ship to move away from a given Position
     */
    fun costToMoveFrom(position: Position): Int = map.at(position)!!.halite / Constants.MOVE_COST_RATIO

    /**
     * Return the Halite that a Ship can currently expect to mine from a given Position.
     * Inspiration is considered by default.
     */
    fun expectedHaliteThisTurn(position: Position, considerInspiration: Boolean = true, distance: Int = 1): Int {
        val haliteToBeMined = ceil(map.at(position)!!.halite / Constants.EXTRACT_RATIO.toDouble()).toInt()
        return if (considerInspiration
                && map.at(position)!!.nearByEnemyShips >= Constants.INSPIRATION_SHIP_COUNT) {
            haliteToBeMined + (INSPIRATION_CONSIDERATION_BONUS * haliteToBeMined) / max(distance, 1)
        } else {
            haliteToBeMined
        }
    }

    /**
     * Return an estimated Expected value of halite from a round trip with 10 turns of mining at a position
     */
    fun expectedRoundTripValue(position: Position): Int {
        val cell = map.at(position)!!
        return (cell.halite - averageCostToMoveFrom) - (cell.nearestDropoffDistance!! * averageCostToMoveFrom)
    }

    /**
     * Returns a list of all positions where Halite can be dropped off, including the shipyard
     */
    fun allCurrentDropoffPositions(): Set<Position> {
        return this.dropoffs.values
                .map { it.position }
                .plus(this.shipyard.position)
                .toMutableSet()
    }

    /**
     * Return the Position of the structure nearest to `position` that
     * halite can be dropped off at.
     */
    fun nearestDropoffPosition(position: Position): Position {
        return dropoffs.mapValues { it.value.position }
                .mapKeys { map.calculateDistance(position, it.value) }
                .plus(map.calculateDistance(position, shipyard.position)
                        to shipyard.position)
                .minBy { it.key }!!
                .value
    }

    /**
     * Updates the position/distance of the nearest dropoff from each cell
     */
    fun setNearestDropoffs() {
        map.cells.forEach { row ->
            row.forEach { cell ->
                cell.nearestDropoffPosition = nearestDropoffPosition(cell.position)
                cell.nearestDropoffDistance = map.calculateDistance(cell.position, cell.nearestDropoffPosition!!)
            }
        }
    }
}


