package bot.analysis

import hlt.GameMap
import org.apache.commons.math3.ml.clustering.DBSCANClusterer

/**
 * Finds clusters of higher than average Halite in provided MapGrid
 */
data class MapClusterer(val map: GameMap, val filterHaliteCutoff: Int, private val minClusterSize: Int = 1) {
    private val clusterableCells = map.cells.flatMap { row ->
        row.map{
            ClusterableMapCell(it)
        }
    }
    private val filteredCells = clusterableCells.filter { it.cell.halite >= filterHaliteCutoff }

    private val clusterer = DBSCANClusterer<ClusterableMapCell>(1.0, minClusterSize)

    /**
     * List of clusters in the provided MapGrid
     */
    val clusters = clusterer.cluster(filteredCells).map{BoundedCluster(it)}
}

