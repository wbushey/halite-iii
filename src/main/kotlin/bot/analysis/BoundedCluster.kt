package bot.analysis

import org.apache.commons.math3.ml.clustering.Cluster

class BoundedCluster (val cluster: Cluster<ClusterableMapCell>){
    val minX = cluster.points.map{it.cell.position.x}.min()!!
    val maxX = cluster.points.map{it.cell.position.x}.max()!!
    val midX = (minX + maxX) / 2
    val minY = cluster.points.map{it.cell.position.y}.min()!!
    val maxY = cluster.points.map{it.cell.position.y}.max()!!
    val midY = (minY + maxY) / 2
}