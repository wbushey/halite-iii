package bot.analysis

import hlt.MapCell
import org.apache.commons.math3.ml.clustering.Clusterable

data class ClusterableMapCell(val cell: MapCell): Clusterable{
    override fun getPoint(): DoubleArray = doubleArrayOf(cell.position.x.toDouble(), cell.position.y.toDouble())
}
