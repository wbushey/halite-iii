package bot.analysis

import hlt.Position

/**
 * Helper class to keep track of the X best cells based on some comparator
 */
data class BestPositions(val size: Int, val initialPosition: Position, val cellComparer: Comparator<Position>) {
    val cells: Array<Position> = Array(size) { initialPosition }

    fun conditionallyInsert(position: Position) {
        if (cellComparer.compare(position, cells[0]) > 0) {
            cells[0] = position
            cells.sortWith(cellComparer)
        }
    }
}
