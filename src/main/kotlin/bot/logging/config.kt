package bot.logging

import hlt.PlayerId
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory

fun configLogger(botId: PlayerId) {
    val logLevel = Level.getLevel(System.getProperty("log.level", "error").toUpperCase())
    val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
    builder.setStatusLevel(Level.ERROR)
    builder.setPackages("bot")
    builder.setConfigurationName("HaliteConfig")
    val rootLogger = builder.newAsyncRootLogger(logLevel)

    // Root Appender
    val rootFile = builder.newAppender("RootFile", "File")
            .addAttribute("append", false)
            .addAttribute("fileName", "bot-${botId.id}.log")
            .add(
                    builder.newLayout("PatternLayout")
                            .addAttribute("pattern", "%r [%t] %-5p Turn: %-3X{turnNumber} - %m%n"))
    builder.add(rootFile)
    rootLogger.add(builder.newAppenderRef("RootFile"))

    // FLog Appender
    // Only added if log level is at INFO or below
    if (logLevel.isLessSpecificThan(Level.INFO)) {
        val flogFile = builder.newAppender("FLogFile", "File")
                .addAttribute("append", false)
                .addAttribute("fileName", "/tmp/flogs/bot-${botId.id}.json")
                .add(builder.newFilter("FLogFilter", Filter.Result.ACCEPT, Filter.Result.DENY))
                .add(
                        builder.newLayout("PatternLayout")
                                .addAttribute("pattern", "{\"t\": %X{turnNumber}, \"x\": %X{position-x}, " +
                                        "\"y\": %X{position-y}, \"msg\": \"[%X{botId}] %m<br/>\"},%n"))
        builder.add(flogFile)
        rootLogger.add(builder.newAppenderRef("FLogFile"))
    }

    builder.add(rootLogger)
    Configurator.initialize(builder.build())
}
