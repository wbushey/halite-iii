package bot.logging

import org.apache.logging.log4j.Logger

/**
 * Idiomatic logging in Kotlin
 * Mostly copied from Tyler's copying of `kotlin-logging`
 */
class KLogger(private val underlyingLogger: Logger): Logger by underlyingLogger {
    fun debug(msg: () -> String) {
        if (isDebugEnabled) debug(msg.invoke())
    }

    fun info(msg: () -> String) {
        if (isInfoEnabled) info(msg.invoke())
    }

    fun warn(msg: () -> String) {
        if (isInfoEnabled) info(msg.invoke())
    }

    fun error(msg: () -> String) {
        if (isErrorEnabled) error(msg.invoke())
    }

    fun fatal(msg: () -> String) {
        if (isFatalEnabled) fatal(msg.invoke())
    }

    fun debug(t: Throwable?, msg: () -> String) {
        if (isDebugEnabled) debug(msg.invoke(), t)
    }

    fun info(t: Throwable?, msg: () -> String) {
        if (isInfoEnabled) info(msg.invoke(), t)
    }

    fun warn(t: Throwable?, msg: () -> String) {
        if (isInfoEnabled) info(msg.invoke(), t)
    }


    fun error(t: Throwable?, msg: () -> String) {
        if (isErrorEnabled) error(msg.invoke(), t)
    }

    fun fatal(t: Throwable?, msg: () -> String) {
        if (isFatalEnabled) fatal(msg.invoke(), t)
    }
}
