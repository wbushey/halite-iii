package bot.logging

import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.LogEvent
import org.apache.logging.log4j.core.config.plugins.Plugin
import org.apache.logging.log4j.core.filter.AbstractFilter
import org.apache.logging.log4j.util.ReadOnlyStringMap
import org.apache.logging.log4j.core.config.plugins.PluginAttribute
import org.apache.logging.log4j.core.config.plugins.PluginFactory

/**
 * Filter that determines whether a log message has the relevant ContextMap data to be written to an F-Log
 */
@Plugin(name = "FLogFilter", category = "Core", elementType = "filter", printObject = true)
class FLogFilter private constructor(onMatch: Filter.Result,
                                     onMismatch: Filter.Result) : AbstractFilter(onMatch, onMismatch) {

    override fun filter(event: LogEvent): Filter.Result {
        return filter(event.contextData)
    }

    private fun filter(contextData: ReadOnlyStringMap): Filter.Result {
        val hasRequiredContextData = listOf("turnNumber", "position-x", "position-y")
                                     .all { contextData.containsKey(it) }
        return if (hasRequiredContextData) onMatch else onMismatch
    }

    companion object {
        @PluginFactory
        @JvmStatic //Factory MUST be a Java static method
        fun createFilter(@PluginAttribute(value = "onMatch", defaultString = "NEUTRAL") onMatch: Filter.Result,
                         @PluginAttribute(value = "onMismatch", defaultString = "DENY") onMismatch: Filter.Result)
                : FLogFilter {
            return FLogFilter(onMatch, onMismatch)
        }
    }
}
