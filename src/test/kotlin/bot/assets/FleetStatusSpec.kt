package bot.assets

import hlt.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import testHelpers.fleetStatusFixture
import testHelpers.resetConstants

object FleetStatusSpec : Spek({
    val playerId = PlayerId(1)
    val fleetStatus = fleetStatusFixture(playerId)

    // Set the fleet for each test
    beforeEachTest {
        resetConstants()
        fleetStatus.map.cells[1][1].halite = 0

        fleetStatus.dropoffs.apply {
            putAll(
                    linkedMapOf(
                            dropoffPair(200, Position(2, 2), playerId),
                            dropoffPair(700, Position(7, 7), playerId)
                    ))
        }
    }

    context("costToMoveFrom") {
        it("returns the cost to move from a Position") {
            fleetStatus.map.cells[1][1].halite = 100
            assertEquals(10, fleetStatus.costToMoveFrom(Position(1, 1)))

            // Cost always truncates down to an Int
            fleetStatus.map.cells[1][1].halite = 91
            assertEquals(9, fleetStatus.costToMoveFrom(Position(1, 1)))
            fleetStatus.map.cells[1][1].halite = 99
            assertEquals(9, fleetStatus.costToMoveFrom(Position(1, 1)))

            // Cost can be zero
            fleetStatus.map.cells[1][1].halite = 9
            assertEquals(0, fleetStatus.costToMoveFrom(Position(1, 1)))
        }
    }

    context("expectedHaliteThisTurn") {
        it("returns the Halite expected to be mined at a Position") {
            fleetStatus.map.cells[1][1].halite = 100
            assertEquals(25, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))

            // Minded Halite always rounds up to an Int
            fleetStatus.map.cells[1][1].halite = 96
            assertEquals(24, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))
            fleetStatus.map.cells[1][1].halite = 95
            assertEquals(24, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))
            fleetStatus.map.cells[1][1].halite = 93
            assertEquals(24, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))

            // Expect zero from zero
            fleetStatus.map.cells[1][1].halite = 0
            assertEquals(0, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))
        }

        it("returns the inspired value of Halite for a Position that is near 2 or more enemy ships") {
            val unInspiredHalite = 25
            val inspiredHalite = 275
            fleetStatus.map.cells[1][1].halite = 100
            fleetStatus.map.cells[1][1].nearByEnemyShips = 0
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 1
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 2
            assertEquals(inspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 3
            assertEquals(inspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1)))
        }

        it("returns the uninspired value of Halite if considerHalite = false, even if enemy ships are near by") {
            val unInspiredHalite = 25
            fleetStatus.map.cells[1][1].halite = 100
            fleetStatus.map.cells[1][1].nearByEnemyShips = 0
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1), false))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 1
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1), false))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 2
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1), false))

            fleetStatus.map.cells[1][1].nearByEnemyShips = 3
            assertEquals(unInspiredHalite, fleetStatus.expectedHaliteThisTurn(Position(1, 1), false))

        }
    }

    context("nearestDropoffPosition") {
        it("returns the position of the nearest dropoff") {
            // Positions 1,1; 1,2; and 2,1 should be closest to dropoff 22 by 2, 1, and 1
            assertEquals(Position(2, 2), fleetStatus.nearestDropoffPosition(Position(1, 1)))

            assertEquals(Position(2, 2), fleetStatus.nearestDropoffPosition(Position(1, 2)))

            assertEquals(Position(2, 2), fleetStatus.nearestDropoffPosition(Position(2, 1)))

            // Position 6,6 should be 2 away from dropoff 77
            assertEquals(Position(7, 7), fleetStatus.nearestDropoffPosition(Position(6, 6)))

            // Position 4,5 is equidistant by 5 to both dropoffs
            assertTrue(arrayOf(
                    Position(2, 2),
                    Position(7, 7)).contains(fleetStatus.nearestDropoffPosition(Position(4, 5))))

            // Position 63,1 should be 1 away from the Shipyard at 63,0
            assertEquals(Position(63, 0), fleetStatus.nearestDropoffPosition(Position(63, 1)))

            // Position 2, 63 should be closest to dropoff 22 via the circular grid
            assertEquals(Position(2, 2), fleetStatus.nearestDropoffPosition(Position(2, 63)))

            // Position 63, 63 should be 1 away from the shipyard via the circular grid
            assertEquals(Position(63, 0), fleetStatus.nearestDropoffPosition(Position(63, 63)))

            // Positions that are on dropoffs or shipyards should be 0 away from a dropoff
            assertEquals(Position(2, 2), fleetStatus.nearestDropoffPosition(Position(2, 2)))
            assertEquals(Position(7, 7), fleetStatus.nearestDropoffPosition(Position(7, 7)))
            assertEquals(Position(63, 0), fleetStatus.nearestDropoffPosition(Position(63, 0)))

        }
    }

    context("targetNumberOfDropoffs") {
        it("increases with map size") {
            Constants.MAP_WIDTH = 32
            Constants.MAP_HEIGHT = 32
            val sMapTarget = fleetStatusFixture(playerId).targetNumberOfDropoffs

            Constants.MAP_WIDTH = 40
            Constants.MAP_HEIGHT = 40
            val mMapTarget = fleetStatusFixture(playerId).targetNumberOfDropoffs

            Constants.MAP_WIDTH = 48
            Constants.MAP_HEIGHT = 48
            val lMapTarget = fleetStatusFixture(playerId).targetNumberOfDropoffs

            Constants.MAP_WIDTH = 56
            Constants.MAP_HEIGHT = 56
            val xlMapTarget = fleetStatusFixture(playerId).targetNumberOfDropoffs

            Constants.MAP_WIDTH = 64
            Constants.MAP_HEIGHT = 64
            val xxlMapTarget = fleetStatusFixture(playerId).targetNumberOfDropoffs

            assertTrue(sMapTarget <= mMapTarget)
            assertTrue(mMapTarget <= lMapTarget)
            assertTrue(lMapTarget <= xlMapTarget)
            assertTrue(xlMapTarget <= xxlMapTarget)

        }
    }
})

fun dropoffPair(id: Int, position: Position, playerId: PlayerId): Pair<EntityId, DropOff> {
    val entityId = EntityId(id)
    return entityId to DropOff(playerId, entityId, position)
}
