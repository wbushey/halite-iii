package bot.executives.decisions

import bot.planning.Mission
import bot.planning.Step
import hlt.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*
import kotlin.random.Random

object PossibleDirectionsSpec : Spek({
    val playerId = PlayerId(1)
    val ship = Ship(playerId, EntityId(10), Position(1, 1), 100)
    val nearestDropoffPosition = Position(30, 30)
    val mission = Mission.DoNothing()
    val map = GameMap(Constants.MAP_WIDTH, Constants.MAP_HEIGHT)

    beforeEachTest {
        map.clearMarks()
    }

    it("returns all possible moves if no obstructions") {
        assertEquals(listOf(Direction.STILL) + Direction.ALL_CARDINALS,
                map.possibleDirections(ship, mission, nearestDropoffPosition, 0))
    }

    it("does not include stay if the ship is on the player's shipyard or dropoff") {
        assertFalse(map.possibleDirections(ship, mission, Position(1, 1), 0).contains(Direction.STILL))
    }

    it("returns only Stay if ship doesn't have enough Halite to move") {
        val ship = Ship(playerId, EntityId(10), Position(1, 0), 1)
        map.at(Position(1, 0))!!.halite = 50
        assertEquals(listOf(Direction.STILL), map.possibleDirections(ship, mission, nearestDropoffPosition, 10))
    }


    context("with RushToDropoff mission") {
        val mission = Mission.RushToDropoff()

        it("Does not include moves into known unsafe positions") {
            assertSafeDirections(ship, map, mission, nearestDropoffPosition)
        }

        it("After some time, includes moves into positions that might contain enemy ships"){
            assertLessSafeDirectionsOverTime(ship, map, mission, nearestDropoffPosition)
        }

        it("will move into an occupied dropoff") {
            // Make a ship right next to a dropoff
            val ship = Ship(playerId, EntityId(10), Position(30, 29), 1)

            // Indicate that another ship is/will be in the dropoff
            map.at(nearestDropoffPosition)!!.markWillBeOccupiedBy(
                    Ship(ship.owner, EntityId(Random.nextInt()), nearestDropoffPosition, 0))

            assertEquals(setOf(Direction.SOUTH), map.possibleDirections(ship, mission, nearestDropoffPosition, 0).toSet())
        }
    }

    context("with GoTo step"){
        val mission = Mission.DoNothing()
        beforeGroup{
            mission.replaceCurrentStep(Step.GoTo(Position(10, 10)))
        }

        it("Does not include moves into known unsafe positions") {
            assertSafeDirections(ship, map, mission, nearestDropoffPosition)
        }

        it("After some time, includes moves into positions that might contain enemy ships"){
            assertLessSafeDirectionsOverTime(ship, map, mission, nearestDropoffPosition)
        }

    }

    context("with GoToDropoff step"){
        val mission = Mission.DoNothing()
        beforeGroup{
            mission.replaceCurrentStep(Step.GoToDropoff(Position(30, 30)))
        }

        it("Does not include moves into known unsafe positions") {
            assertSafeDirections(ship, map, mission, nearestDropoffPosition)
        }

        it("After some time, includes moves into positions that might contain enemy ships"){
            assertLessSafeDirectionsOverTime(ship, map, mission, nearestDropoffPosition)
        }

        it("will move into a dropoff and surrounding positions if the dropoff is occupied by an enemy ship"){
            // Make a ship two moves away from a a dropoff
            var ship = Ship(playerId, EntityId(10), Position(30, 28), 100)

            // Indicate that an enemy ship may be on the dropoff or next to it
            val enemyShip = Ship(PlayerId(Random.nextInt()), EntityId(Random.nextInt()), nearestDropoffPosition, 0)
            Direction.ALL_DIRECTIONS.forEach { direction ->
                map.at(nearestDropoffPosition.directionalOffset(direction))!!.markWillBeOccupiedBy(enemyShip)
            }
            assertTrue(map.possibleDirections(ship, mission, nearestDropoffPosition, 0).contains(Direction.SOUTH))

            // Move the ship to right next to the dropoff
            ship = Ship(playerId, EntityId(10), Position(30, 29), 100)
            assertTrue(map.possibleDirections(ship, mission, nearestDropoffPosition, 0).contains(Direction.SOUTH))
        }
    }

    context("with GoTo step"){
        val mission = Mission.DoNothing()
        beforeGroup{
            mission.replaceCurrentStep(Step.Collect())
        }

        it("Does not include moves into known unsafe positions at first") {
            assertSafeDirections(ship, map, mission, nearestDropoffPosition)
        }

        it("After some time, includes moves into positions that might contain enemy ships"){
            assertLessSafeDirectionsOverTime(ship, map, mission, nearestDropoffPosition)
        }
    }
})

fun assertSafeDirections(ship: Ship, map: GameMap, mission: Mission, nearestDropoffPosition: Position) {
    val northPosition = ship.position.directionalOffset(Direction.NORTH)
    map.at(northPosition)!!.markWillBeOccupiedBy(Ship(ship.owner, EntityId(Random.nextInt()), northPosition, 0))
    val currentPosition = ship.position
    map.at(currentPosition)!!.markWillBeOccupiedBy(Ship(ship.owner, EntityId(Random.nextInt()), currentPosition, 0))

    assertEquals(setOf(Direction.EAST, Direction.WEST, Direction.SOUTH),
            map.possibleDirections(ship, mission, nearestDropoffPosition, 0).toSet())
}

fun assertLessSafeDirectionsOverTime(ship: Ship, map: GameMap, mission: Mission, nearestDropoffPosition: Position) {
    val otherPlayerId = PlayerId(9999)
    val northPosition = ship.position.directionalOffset(Direction.NORTH)
    val southPosition = ship.position.directionalOffset(Direction.SOUTH)
    val eastPosition = ship.position.directionalOffset(Direction.EAST)
    val westPosition = ship.position.directionalOffset(Direction.WEST)

    // Indicate that enemy ships will be in North and South positions
    map.at(northPosition)!!.markWillBeOccupiedBy(Ship(otherPlayerId, EntityId(Random.nextInt()), northPosition, 0))
    map.at(southPosition)!!.markWillBeOccupiedBy(Ship(otherPlayerId, EntityId(Random.nextInt()), southPosition, 0))

    // Indicate that friendly ships will be East and West positions
    map.at(eastPosition)!!.markWillBeOccupiedBy(Ship(ship.owner, EntityId(Random.nextInt()), eastPosition, 0))
    map.at(westPosition)!!.markWillBeOccupiedBy(Ship(ship.owner, EntityId(Random.nextInt()), westPosition, 0))

    assertEquals(setOf(Direction.NORTH, Direction.SOUTH, Direction.STILL),
            map.possibleDirections(ship, mission, nearestDropoffPosition, 10).toSet())
}
