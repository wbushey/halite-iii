package bot.executives

import bot.assets.FleetStatus
import bot.assets.dropoffPair
import bot.planning.Mission
import hlt.*
import io.mockk.every
import io.mockk.mockk
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*
import testHelpers.fleetStatusFixture

object CommanderSpec : Spek({
    val meId = PlayerId(1)
    lateinit var fleetStatus: FleetStatus
    lateinit var subject: Commander
    lateinit var aShip: Ship

    beforeEachTest {
        fleetStatus = fleetStatusFixture()
        subject = Commander(fleetStatus)
        aShip = createShipAt(Position(0, 0), fleetStatus, meId)
        subject.update()
    }

    describe("update") {
        it("adds captains for new ships") {
            assertEquals(setOf(aShip.id), subject.captains.keys)

            val anotherShip = createShipAt(Position(0, 0), fleetStatus, meId)
            subject.update()
            assertEquals(setOf(aShip.id, anotherShip.id), subject.captains.keys)
        }

        it("removes captains for destroyed ships") {
            assertEquals(setOf(aShip.id), subject.captains.keys)

            fleetStatus.ships.remove(aShip.id)
            subject.update()
            assertEquals(setOf<EntityId>(), subject.captains.keys)
        }

        context("with Captains in need of Missions") {
            context("and dropoffs not buildable") {
                beforeEachTest {
                    fleetStatus.dropoffs.putAll(
                            linkedMapOf(dropoffPair(200, Position(2, 2), meId),
                                    dropoffPair(700, Position(7, 7), meId),
                                    dropoffPair(3000, Position(30, 30), meId),
                                    dropoffPair(4000, Position(40, 40), meId)
                            )
                    )
                }
                it("assigns Captains to Gather farther away as time advances") {
                    val captain = subject.captains.values.first()
                    fleetStatus.map.at(Position(0, 1))!!.halite = 100
                    fleetStatus.map.at(Position(0, 5))!!.halite = 300
                    fleetStatus.map.at(Position(0, 30))!!.halite = 900

                    fleetStatus.turnNumber = 1
                    completeMission(captain)
                    subject.update()
                    assertEquals(Position(0, 1), (captain.mission as Mission.Gather).target)

                    fleetStatus.turnNumber = 200
                    completeMission(captain)
                    subject.update()
                    assertEquals(Position(0, 5), (captain.mission as Mission.Gather).target)

                    fleetStatus.turnNumber = 400
                    completeMission(captain)
                    subject.update()
                    assertEquals(Position(0, 30), (captain.mission as Mission.Gather).target)
                }

                it("assigns Captains to Gather from the highest Halite cells") {
                    val captain = subject.captains.values.first()
                    val captsPosition = captain.ship.position
                    val highHaliteCell = fleetStatus.map.at(Position(captsPosition.x + 3, captsPosition.y + 2))!!
                    highHaliteCell.halite = 900

                    fleetStatus.turnNumber = 400
                    completeMission(captain)
                    subject.update()
                    assertEquals(highHaliteCell.position, (captain.mission as Mission.Gather).target)
                }
            }

            context("with dropoffs buildable") {
                beforeEachTest {
                    // Build a ship far from things
                    createShipAt(Position(5, 48), fleetStatus, meId)

                    // Put a cluster around this new ship
                    fleetStatus.map.at(Position(4, 48))!!.halite = 700
                    fleetStatus.map.at(Position(5, 47))!!.halite = 700
                    fleetStatus.map.at(Position(5, 48))!!.halite = 700
                    fleetStatus.map.at(Position(5, 49))!!.halite = 700
                    fleetStatus.map.at(Position(6, 48))!!.halite = 700
                    subject.update()
                }
                it("builds a dropoff using a ship that is a healthy distance from existing dropoffs") {
                    fleetStatus.turnNumber = 400
                    fleetStatus.halite = 10000
                    subject.update()
                    val dropoffCaptains = subject.captains.values.filter { it.mission == Mission.CreateDropoff }
                    assertTrue(dropoffCaptains.isNotEmpty())

                    val allDropoffPositions = fleetStatus.dropoffs.values.map { it.position }.plus(fleetStatus.shipyard.position)
                    dropoffCaptains.forEach { captain ->
                        allDropoffPositions.forEach { dropoffPosition ->
                            assertTrue(fleetStatus.map.calculateDistance(dropoffPosition, captain.ship.position) >= 8, "Ship position is ${captain.ship.position}, dropoff's position is $dropoffPosition")
                        }
                    }
                }

                it("does not build if a ship is on another dropoff") {
                    subject.captains.values.map { it.ship.position }.forEach {
                        fleetStatus.map.at(it)!!.structure = DropOff(PlayerId(20), EntityId(5000), it)
                    }
                    fleetStatus.turnNumber = 400
                    fleetStatus.halite = 10000
                    subject.update()
                    val dropoffCaptains = subject.captains.values.filter { it.mission == Mission.CreateDropoff }
                    assertTrue(dropoffCaptains.isEmpty())

                }
            }
        }

        context("near game end") {
            /**
             * Shipyard is at Position(63,0)
             * Ship created above is at Position(0,0), 1 step away from Shipyard
             * Creating ship at Farthest away position: Position(31,32), 64 steps away
             * Creating ship somewhat far away: Position(10,10), 21 steps away
             */
            lateinit var farShip: Ship
            lateinit var midShip: Ship
            beforeEachTest {
                farShip = createShipAt(Position(31, 32), fleetStatus, meId)
                midShip = createShipAt(Position(10, 10), fleetStatus, meId)
            }

            it("gives Captains a RushToDropoff mission based on Captain distance and remaining gametime") {
                fleetStatus.turnNumber = 436
                subject.update()
                assertTrue(subject.captains[farShip.id]!!.mission is Mission.RushToDropoff)
                assertFalse(subject.captains[midShip.id]!!.mission is Mission.RushToDropoff)
                assertFalse(subject.captains[aShip.id]!!.mission is Mission.RushToDropoff)

                fleetStatus.turnNumber = 480
                subject.update()
                assertTrue(subject.captains[farShip.id]!!.mission is Mission.RushToDropoff)
                assertTrue(subject.captains[midShip.id]!!.mission is Mission.RushToDropoff)
                assertFalse(subject.captains[aShip.id]!!.mission is Mission.RushToDropoff)

                fleetStatus.turnNumber = 499
                subject.update()
                assertTrue(subject.captains[farShip.id]!!.mission is Mission.RushToDropoff)
                assertTrue(subject.captains[midShip.id]!!.mission is Mission.RushToDropoff)
                assertTrue(subject.captains[aShip.id]!!.mission is Mission.RushToDropoff)
            }
        }

    }

    describe("prioritized") {
        it("creates a list of Captains in order of number of possible moves, then ship ID, lowest first") {
            val oneMoveShipOneCaptain = mockk<Captain>()
            every { oneMoveShipOneCaptain.possibleDirections.count() } returns 1
            every { oneMoveShipOneCaptain.ship.id } returns EntityId(1)
            val oneMoveShipTwoCaptain = mockk<Captain>()
            every { oneMoveShipTwoCaptain.possibleDirections.count() } returns 1
            every { oneMoveShipTwoCaptain.ship.id } returns EntityId(2)
            val oneMoveShipThreeCaptain = mockk<Captain>()
            every { oneMoveShipThreeCaptain.possibleDirections.count() } returns 1
            every { oneMoveShipThreeCaptain.ship.id } returns EntityId(3)
            val twoMoveCaptain = mockk<Captain>()
            every { twoMoveCaptain.possibleDirections.count() } returns 2
            every { twoMoveCaptain.ship.id } returns EntityId(4)
            val threeMoveCaptain = mockk<Captain>()
            every { threeMoveCaptain.possibleDirections.count() } returns 3
            every { threeMoveCaptain.ship.id } returns EntityId(5)

            val captains = mutableMapOf(
                    oneMoveShipTwoCaptain.ship.id to oneMoveShipTwoCaptain,
                    twoMoveCaptain.ship.id to twoMoveCaptain,
                    oneMoveShipThreeCaptain.ship.id to oneMoveShipThreeCaptain,
                    threeMoveCaptain.ship.id to threeMoveCaptain,
                    oneMoveShipOneCaptain.ship.id to oneMoveShipOneCaptain)
            assertEquals(listOf(oneMoveShipOneCaptain,
                    oneMoveShipTwoCaptain,
                    oneMoveShipThreeCaptain,
                    twoMoveCaptain,
                    threeMoveCaptain),
                    captains.prioritized())

        }
    }

    describe("shouldBuildShip") {
        context("early in game with enough Halite and an unblocked shipyard") {
            beforeEachTest {
                fleetStatus.turnNumber = 200
                fleetStatus.halite = 10000
            }
            it("returns true") {
                assertTrue(shouldBuildShip(fleetStatus))

            }
        }

        context("early in game with out enough Halite and an unblocked shipyard") {
            beforeEachTest {
                fleetStatus.turnNumber = 200
                fleetStatus.halite = 10
            }
            it("returns false") {
                assertFalse(shouldBuildShip(fleetStatus))

            }
        }


    }

    describe("numberOfDropoffsToBuild") {
        context("later in the game with enough Halite and no dropoffs built") {
            beforeEachTest {
                fleetStatus.turnNumber = 400
                fleetStatus.halite = 4000
            }

            it("returns number of dropoffs to build based on available halite") {
                assertEquals(1, numberOfDropoffsToBuild(fleetStatus))
                fleetStatus.halite = 8000
                assertEquals(2, numberOfDropoffsToBuild(fleetStatus))
                fleetStatus.halite = 12000
                assertEquals(3, numberOfDropoffsToBuild(fleetStatus))
                fleetStatus.halite = 16000
                assertEquals(4, numberOfDropoffsToBuild(fleetStatus))
            }
        }

        context("later in the game with enough Halite and target dropoffs reached") {
            beforeEachTest {
                fleetStatus.turnNumber = 400
                fleetStatus.halite = 4000
                fleetStatus.dropoffs.putAll(listOf(
                        EntityId(1) to DropOff(meId, EntityId(1), Position(20, 20)),
                        EntityId(2) to DropOff(meId, EntityId(2), Position(30, 30)),
                        EntityId(3) to DropOff(meId, EntityId(3), Position(30, 30)),
                        EntityId(4) to DropOff(meId, EntityId(4), Position(50, 50))
                ))
            }

            it("returns 0") {
                assertEquals(0, numberOfDropoffsToBuild(fleetStatus))
            }

        }
    }
})

fun completeMission(captain: Captain) {
    while (!captain.mission.isComplete) captain.mission.advance()
}
