package bot.executives

import bot.assets.FleetStatus
import bot.executives.decisions.possibleDirections
import bot.planning.Mission
import bot.planning.Step
import hlt.*
import io.mockk.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*
import testHelpers.fleetStatusFixture
import kotlin.math.ceil
import kotlin.random.Random


object CaptainSpec : Spek({
    val playerId = PlayerId(1)

    var fleetStatus = fleetStatusFixture(playerId)
    var captainsFactory = Captain.Factory(fleetStatus)
    lateinit var captain: Captain

    // Set the fleet for each test
    beforeEachTest {
        fleetStatus = fleetStatusFixture(playerId)

        // Add a ship
        fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(1, 1), 1000)
        fleetStatus.map.at(Position(1, 1))!!.markOccupiedBy(fleetStatus.ships[EntityId(11)]!!)

        // Add a dropoff
        fleetStatus.dropoffs[EntityId(33)] = DropOff(playerId, EntityId(33), Position(3, 3))

        captainsFactory = Captain.Factory(fleetStatus)
        captain = captainsFactory.build(EntityId(11), Mission.DoNothing())

        /**
         *  Initial amounts of halite in adjacent squares. Ship is at position 1,1
         *   |  0  |  1  |  2  |  3  |
         *  0| 700 | 900 | 300 |     |
         *  1| 600 | 100 | 500 |     |
         *  2|     | 500 |     |     |
         *  3|     |     |     |  D  |
         */
        // `cells` is YX indexed, while Position is X,Y constructed. This is fine :(
        fleetStatus.map.cells[0][0].halite = 700
        fleetStatus.map.cells[0][1].halite = 900
        fleetStatus.map.cells[0][2].halite = 300
        fleetStatus.map.cells[1][0].halite = 600
        fleetStatus.map.cells[1][2].halite = 500
        fleetStatus.map.cells[2][1].halite = 500
        fleetStatus.map.cells[1][1].halite = 100

    }

    context("ship") {
        it("returns the correct ship from the fleet based on the EntityId") {
            assertEquals(fleetStatus.ships.getValue(EntityId(11)), captain.ship)

            val otherShip = Ship(playerId, EntityId(11), Position(0, 0), 100)
            fleetStatus.ships.put(EntityId(11), otherShip)
            assertEquals(otherShip, captain.ship)
        }
    }

    context("nearestDropoffPosition") {
        it("returns the position of the nearest dropoff or shipyard") {
            assertEquals(Position(63, 0), captain.nearestDropoffPosition)
        }
    }

    context("distanceToNearestDropoff") {
        it("calculates the distance to the nearest dropoff or shipyard") {
            assertEquals(3, captain.distanceToNearestDropoff)
        }
    }

    context("possibleDirections") {
        mockkStatic("bot.executives.decisions.PossibleDirectionsKt")

        it("it's simply a wrapper around the possibleMoves function") {
            captain.possibleDirections
            verify { fleetStatus.map.possibleDirections(captain.ship, captain.mission, captain.nearestDropoffPosition, 0 ) }
        }
    }

    context("chooseCommand") {
        context("with ConvertToDropoff step"){
            val createDropoffMission = Mission.DoNothing()
            lateinit var captain: Captain

            beforeEachTest {
                createDropoffMission.replaceCurrentStep(Step.ConvertToDropoff)
                fleetStatus.map.clearMarks()
                captain = captainsFactory.build(EntityId(11), createDropoffMission)
            }

            it("issues a TransformToDropoff command"){
                assertEquals(Command.transformShipIntoDropoffSite(captain.ship.id), captain.chooseCommand())
            }
        }
        context("with GoTo step") {
            val goToMission = Mission.DoNothing()
            lateinit var captain: Captain
            beforeEachTest {
                goToMission.replaceCurrentStep(Step.GoTo(Position(20, 20)))
                fleetStatus.map.clearMarks()
                captain = captainsFactory.build(EntityId(11), goToMission)
            }

            it("relies on possibleDirections and Ship.moveTo") {
                assertReliesOnPossibleDirectionsAndShipMoveTo(captain, fleetStatus)
            }

            it("will move into a position that is closer to the target by Manhattan distance") {
                // Ship is at 1,1
                assertMovingInSpecificDirection(Step.GoTo(Position(2, 1)), Direction.EAST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoTo(Position(0, 1)), Direction.WEST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoTo(Position(1, 2)), Direction.SOUTH, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoTo(Position(1, 0)), Direction.NORTH, captain, fleetStatus)

                assertMovingInOneOfDirections(Step.GoTo(Position(0, 0)), listOf(Direction.WEST, Direction.NORTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoTo(Position(2, 0)), listOf(Direction.EAST, Direction.NORTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoTo(Position(0, 2)), listOf(Direction.WEST, Direction.SOUTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoTo(Position(2, 2)), listOf(Direction.EAST, Direction.SOUTH), captain, fleetStatus)
            }

            it("will move into a position that is closer to the target by toroidal Manhattan distance") {
                // Ship is at 1,1
                assertMovingInSpecificDirection(Step.GoTo(Position(60, 1)), Direction.WEST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoTo(Position(1, 60)), Direction.NORTH, captain, fleetStatus)

                assertMovingInOneOfDirections(Step.GoTo(Position(60, 60)), listOf(Direction.WEST, Direction.NORTH), captain, fleetStatus)
            }

            it("will return some move even if all moves are unsafe") {
                assertReturnsSomeCommandWhenNoneAreSafe(captain, fleetStatus, playerId)
            }
        }

        context("with GoToCollect step") {
            val goToMission = Mission.DoNothing()
            lateinit var captain: Captain
            beforeEachTest {
                goToMission.replaceCurrentStep(Step.GoToCollect(Position(20, 20)))
                fleetStatus.map.clearMarks()

                // Set the map so the average cost to move is 50
                fleetStatus.map.cells.forEach{ row ->
                    row.forEach{ cell ->
                        cell.halite = 500
                        fleetStatus.map.totalHalite += 500
                    }
                }
                fleetStatus.map.at(Position(1, 1))!!.halite = 10
                captain = captainsFactory.build(EntityId(11), goToMission)
            }

            it("relies on possibleDirections and Ship.moveTo") {
                assertReliesOnPossibleDirectionsAndShipMoveTo(captain, fleetStatus)
            }

            it("will move into a position that is closer to the target by Manhattan distance") {
                // Ship is at 1,1
                assertMovingInSpecificDirection(Step.GoToCollect(Position(2, 1)), Direction.EAST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoToCollect(Position(0, 1)), Direction.WEST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoToCollect(Position(1, 2)), Direction.SOUTH, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoToCollect(Position(1, 0)), Direction.NORTH, captain, fleetStatus)

                assertMovingInOneOfDirections(Step.GoToCollect(Position(0, 0)), listOf(Direction.WEST, Direction.NORTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoToCollect(Position(2, 0)), listOf(Direction.EAST, Direction.NORTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoToCollect(Position(0, 2)), listOf(Direction.WEST, Direction.SOUTH), captain, fleetStatus)
                assertMovingInOneOfDirections(Step.GoToCollect(Position(2, 2)), listOf(Direction.EAST, Direction.SOUTH), captain, fleetStatus)
            }

            it("will move into a position that is closer to the target by toroidal Manhattan distance") {
                // Ship is at 1,1
                assertMovingInSpecificDirection(Step.GoToCollect(Position(60, 1)), Direction.WEST, captain, fleetStatus)
                assertMovingInSpecificDirection(Step.GoToCollect(Position(1, 60)), Direction.NORTH, captain, fleetStatus)

                assertMovingInOneOfDirections(Step.GoToCollect(Position(60, 60)), listOf(Direction.WEST, Direction.NORTH), captain, fleetStatus)
            }

            it("will stay on a position with halite about the average move cost"){
                fleetStatus.map.cells[1][1].halite = 980

                var command = captain.chooseCommand() as Command.Move
                assertEquals(Direction.STILL, command.direction)

                // Don't use while; don't infinite loop. But do use a weird version of return
                repeat(50) {
                    if ((captain.chooseCommand() as Command.Move).direction != Direction.STILL) return@it

                    val haliteToFakeMine = ceil(fleetStatus.map.at(Position(1, 1))!!.halite
                            / Constants.EXTRACT_RATIO.toDouble()).toInt()
                    fleetStatus.map.cells[1][1].halite -= haliteToFakeMine
                    fleetStatus.map.clearMarks()
                }


                val expectedHalite = fleetStatus.expectedHaliteThisTurn(captain.ship.position)
                val fakeAverageCostToMove = 30
                assertTrue(expectedHalite < fakeAverageCostToMove,
                        "Expected Halite ($expectedHalite) must be less than average cost to move")
                assertTrue(expectedHalite > 0,
                        "Expected Halite ($expectedHalite) must be greater than zero")

            }

            it("will return some move even if all moves are unsafe") {
                assertReturnsSomeCommandWhenNoneAreSafe(captain, fleetStatus, playerId)
            }
        }


        context("with Collect step") {
            val collectingMission = Mission.DoNothing()
            val fakeAverageHalite = 300
            val fakeAverageCostToMove = 30
            beforeEachTest {
                collectingMission.replaceCurrentStep(Step.Collect())
                fleetStatus.map.clearMarks()
                fleetStatus.map.totalHalite = fakeAverageHalite * 64 * 64
                captain = captainsFactory.build(EntityId(11), collectingMission)
            }
            it("relies on possibleDirections and Ship.moveTo") {
                assertReliesOnPossibleDirectionsAndShipMoveTo(captain, fleetStatus)
            }

            it("will find and stay on adjacent space with the most Halite") {
                // Cell to the north with 900 has the most halite
                assertEquals(Command.move(EntityId(11), Direction.NORTH), captain.chooseCommand())

                // Make cell to the West have the most Halite
                fleetStatus.map.cells[1][0].halite = 950
                assertEquals(Command.move(EntityId(11), Direction.WEST), captain.chooseCommand())

                // Make cell to the East have the most Halite
                fleetStatus.map.cells[1][2].halite = 960
                assertEquals(Command.move(EntityId(11), Direction.EAST), captain.chooseCommand())

                // Make cell to the South have the most Halite
                fleetStatus.map.cells[2][1].halite = 970
                assertEquals(Command.move(EntityId(11), Direction.SOUTH), captain.chooseCommand())

                // Make current cell have the most Halite
                fleetStatus.map.cells[1][1].halite = 980
                assertEquals(Command.move(EntityId(11), Direction.STILL), captain.chooseCommand())
            }

            it("will find adjacent, safe space with the most Halite") {
                // Indicate a ships will be on Position(1, 0) and Position(1,1)
                createShipAt(Position(1, 0), fleetStatus, playerId)
                createShipAt(Position(1, 1), fleetStatus, playerId)

                // Make ship's current position (1,1) as haliteful as position 1,0
                fleetStatus.map.cells[1][1].halite = 50

                assertEquals(Command.move(EntityId(11), Direction.WEST), captain.chooseCommand())
            }

            it("it will stay on a local maximum until it extracts as much per turn as the average cost to move") {
                // Make the current cell the local maximum
                fleetStatus.map.cells[1][1].halite = 980

                // Don't use while; don't infinite loop. But do use a weird version of return
                repeat(50) {
                    if ((captain.chooseCommand() as Command.Move).direction != Direction.STILL) return@it

                    val haliteToFakeMine = ceil(fleetStatus.map.at(Position(1, 1))!!.halite
                            / Constants.EXTRACT_RATIO.toDouble()).toInt()
                    fleetStatus.map.cells[1][1].halite -= haliteToFakeMine
                    fleetStatus.map.clearMarks()
                }


                val expectedHalite = fleetStatus.expectedHaliteThisTurn(captain.ship.position)
                assertTrue(expectedHalite < fakeAverageCostToMove,
                        "Expected Halite ($expectedHalite) must be less than average cost to move")
                assertTrue(expectedHalite > 0,
                        "Expected Halite ($expectedHalite) must be greater than zero")
            }

            it("will move from a local maximum if it becomes unsafe") {
                // Make the current cell the local maximum
                fleetStatus.map.cells[1][1].halite = 980
                assertEquals(Direction.STILL, (captain.chooseCommand() as Command.Move).direction)

                val newShip = createShipAt(Position(0, 1), fleetStatus, playerId)
                fleetStatus.map.cells[1][1].markWillBeOccupiedBy(newShip)
                assertNotEquals(Direction.STILL, (captain.chooseCommand() as Command.Move).direction)
            }

            it("will return some move even if all moves are unsafe") {
                assertReturnsSomeCommandWhenNoneAreSafe(captain, fleetStatus, playerId)
            }
        }
    }
})

/**
 * Helper functions for this spec
 */
fun createShipAt(position: Position, fleetStatus: FleetStatus, playerId: PlayerId): Ship {
    val id = EntityId("${playerId.id}${position.x}${position.y}${Random.nextInt(1000)}".toInt())
    val newShip = Ship(playerId, id, position, 0)
    fleetStatus.ships[id] = newShip
    fleetStatus.map.at(position)!!.markWillBeOccupiedBy(newShip)
    return newShip
}

/**
 * Shared assertions
 */
fun assertReliesOnPossibleDirectionsAndShipMoveTo(captain: Captain, fleetStatus: FleetStatus) {
    val spiedCaptain = spyk(captain)
    val spiedShip = spyk(spiedCaptain.ship)
    fleetStatus.ships[captain.ship.id] = spiedShip
    val command = spiedCaptain.chooseCommand() as Command.Move
    verify { spiedCaptain.possibleDirections }
    verify { spiedShip.moveTo(command.direction, fleetStatus.map) }
}

fun assertReturnsSomeCommandWhenNoneAreSafe(captain: Captain, fleetStatus: FleetStatus, playerId: PlayerId) {
    // Indicate a ships at all positions on/around the captain
    createShipAt(Position(1, 0), fleetStatus, playerId)
    createShipAt(Position(0, 1), fleetStatus, playerId)
    createShipAt(Position(1, 1), fleetStatus, playerId)
    createShipAt(Position(2, 1), fleetStatus, playerId)
    createShipAt(Position(1, 2), fleetStatus, playerId)
    captain.chooseCommand()
}

fun assertMovingInSpecificDirection(step: Step, targetDirection: Direction, captain: Captain, fleetStatus: FleetStatus) {
    captain.mission = Mission.DoNothing()
    captain.mission.replaceCurrentStep(step)
    var command = captain.chooseCommand() as Command.Move
    assertEquals(targetDirection, command.direction)
    fleetStatus.map.clearMarks()
}

fun assertMovingInOneOfDirections(step: Step, targetDirections: List<Direction>, captain: Captain, fleetStatus: FleetStatus) {
    captain.mission = Mission.DoNothing()
    captain.mission.replaceCurrentStep(step)
    var command = captain.chooseCommand() as Command.Move
    assertTrue(targetDirections.contains(command.direction))
    fleetStatus.map.clearMarks()
}
