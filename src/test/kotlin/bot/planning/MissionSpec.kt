package bot.planning

import hlt.Position
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*

object MissionSpec : Spek({
    context("Base"){
        var mission: Mission = Mission.Gather(Position(0, 0))
        beforeEachTest {
            mission = Mission.Gather(Position(0, 0))
        }

        context("advance"){
            it("advances currentStep"){
                val oldStep = mission.currentStep
                mission.advance()
                assertNotEquals(oldStep, mission.currentStep)

            }
        }

        context("isComplete"){
            it("is True once the Mission has advanced through all it's steps"){
                // Gather mission has 3 steps
                repeat(3){
                    assertFalse(mission.isComplete)
                    mission.advance()
                }

                assertTrue(mission.isComplete)
            }
        }

        context("replaceCurrentStep"){
            it("replaces the currentStep"){
                val newStep = Step.Sit
                val oldStep = mission.currentStep
                assertNotEquals(newStep, oldStep)
                mission.replaceCurrentStep(newStep)
                assertEquals(newStep, mission.currentStep)
            }
        }
    }

    context("Gather"){
        var mission: Mission = Mission.Gather(Position(0, 0))

        it("contains the Steps for gathering Halite"){
            assertEquals(Step.GoToCollect(Position(0, 0)), mission.currentStep)
            assertTrue(mission.currentStep is Step.GoToCollect)
            mission.advance()
            assertEquals(Step.Collect(), mission.currentStep)
            mission.advance()
            assertEquals(Step.DelayedDropoff, mission.currentStep)
            mission.advance()
            assertTrue(mission.isComplete)
        }
    }

    context("RushToDropoff"){
        var mission: Mission = Mission.RushToDropoff()

        it("contains the steps for rushing to a dropoff"){
            assertEquals(Step.DelayedDropoff, mission.currentStep)
            mission.advance()
            assertTrue(mission.isComplete)
        }
    }

    context("CreateDropoff"){
        var mission: Mission = Mission.CreateDropoff

        it("contains the steps for creating a dropoff"){
            assertEquals(Step.ConvertToDropoff, mission.currentStep)
            mission.advance()
            assertTrue(mission.isComplete)

        }
    }
})
