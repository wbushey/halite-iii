package bot.planning

import bot.executives.Captain
import hlt.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*
import testHelpers.fleetStatusFixture
import testHelpers.resetConstants

object StepSpec : Spek({

    val playerId = PlayerId(1)

    val fleetStatus = fleetStatusFixture(playerId)
    val captainsFactory = Captain.Factory(fleetStatus)
    lateinit var captain: Captain

    beforeEachTest {
        resetConstants()
        fleetStatus.dropoffs.clear()
        fleetStatus.ships.clear()
        fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(1, 1), 0)
        captain = captainsFactory.build(EntityId(11))

        fleetStatus.map.cells[1][1].halite = 5000
    }

    context("GoTo"){
        context("isComplete"){
            it("Is True when the Captain's Ship is at the target Position"){
                val gotoStep = Step.GoTo(Position(5, 5))
                assertFalse(gotoStep.isComplete(captain, fleetStatus))

                fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(5, 5), 0)
                assertTrue(gotoStep.isComplete(captain, fleetStatus))
            }
        }
    }

    context("GoToCollect"){
        context("isComplete"){
            it("Is True when the Ship is full OR the Ship is at the target Position"){
                val gotoStep = Step.GoToCollect(Position(5, 5))
                assertFalse(gotoStep.isComplete(captain, fleetStatus))

                fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(5, 5), 0)
                assertTrue(gotoStep.isComplete(captain, fleetStatus))


                fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(0, 0), Constants.MAX_HALITE)
                assertTrue(gotoStep.isComplete(captain, fleetStatus))
            }
        }
    }

    context("Collect"){
        val collectStep = Step.Collect()
        context("isComplete"){
            it("is False while the ship is not full and there is Halite to collect"){
                assertFalse(collectStep.isComplete(captain, fleetStatus))
            }

            it("is True when the Ship contains all of the Halite it can hold"){
                fleetStatus.ships[EntityId(11)] = Ship(playerId, EntityId(11), Position(1, 1), Constants.MAX_HALITE)
                assertTrue(collectStep.isComplete(captain, fleetStatus))
            }

            it("is True when the Position contains 0 Halite"){
                fleetStatus.map.cells[1][1].halite = 0
                assertTrue(collectStep.isComplete(captain, fleetStatus))
            }
        }
    }

    context("Sit"){
        context("isComplete"){
            it("is never complete"){
                val sitStep = Step.Sit
                assertFalse(sitStep.isComplete(captain, fleetStatus))
            }
        }
    }

    context("DelayedDropoff"){
        var mission = Mission.DoNothing()
        val delayedDropoffStep = Step.DelayedDropoff

        beforeEachTest {
            mission = Mission.DoNothing()
        }

        context("isComplete"){
            it("is always False"){
                assertFalse(delayedDropoffStep.isComplete(captain, fleetStatus))
            }
        }

        context("update"){
            it("returns a GoToDropoff step with the provided Position"){
                val newStep: Step.GoToDropoff = delayedDropoffStep.update(mission){ Position(3, 3) }
                assertEquals(Position(3, 3), newStep.target)
            }

            it("updates the current step in a Mission to be a GoTo step with the provided Position"){
                delayedDropoffStep.update(mission){ Position(3, 3) }
                assertTrue(mission.currentStep is Step.GoToDropoff)
                val missionCurrentStep= mission.currentStep as Step.GoTo
                assertEquals(Position(3, 3), missionCurrentStep.target)
            }

        }
    }

    context("ConvertToDropoff"){
        context("isComplete"){
            it("when the dropoff has been created"){
                val convertToDropoffStep = Step.ConvertToDropoff
                assertFalse(convertToDropoffStep.isComplete(captain, fleetStatus))

                // Place dropoff on captain's current position
                fleetStatus.dropoffs[EntityId(1101)] = DropOff(PlayerId(0), EntityId(1101), Position(1, 1))
                assertTrue(convertToDropoffStep.isComplete(captain, fleetStatus))
            }
        }
    }
})
