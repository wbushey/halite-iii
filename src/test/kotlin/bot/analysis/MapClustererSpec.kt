package bot.analysis

import hlt.GameMap
import hlt.Position
import org.apache.commons.math3.ml.clustering.Cluster
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue

fun GameMap.setHalite(position: Position, halite: Int) {
    val cell = this.at(position)!!
    this.totalHalite -= cell.halite
    cell.halite = halite
    this.totalHalite += halite
}

object MapClustererSpec : Spek({
    val gameMap = GameMap(64, 64)

    describe("MapClusterer") {

        beforeEachTest {
            // Put clusters around 0,0,; 20, 10; and 35, 35
            gameMap.setHalite(Position(0, -1), 710)
            gameMap.setHalite(Position(-1, 0), 800)
            gameMap.setHalite(Position(0, 0), 700)
            gameMap.setHalite(Position(1, 0), 500)
            gameMap.setHalite(Position(0, 1), 600)

            gameMap.setHalite(Position(20, 9), 710)
            gameMap.setHalite(Position(19, 10), 800)
            gameMap.setHalite(Position(20, 10), 700)
            gameMap.setHalite(Position(21, 10), 500)
            gameMap.setHalite(Position(20, 11), 600)

            gameMap.setHalite(Position(35, 34), 540)
            gameMap.setHalite(Position(34, 35), 825)
            gameMap.setHalite(Position(35, 35), 650)
            gameMap.setHalite(Position(36, 35), 410)
            gameMap.setHalite(Position(35, 36), 780)
        }

        it("returns a list of clusters of high Halite positions") {

            val expectedClusters = listOf(Cluster<ClusterableMapCell>(), Cluster(), Cluster())
            expectedClusters[0].addPoint(ClusterableMapCell(gameMap.at(Position(0, 0))!!))
            expectedClusters[0].addPoint(ClusterableMapCell(gameMap.at(Position(1, 0))!!))
            expectedClusters[0].addPoint(ClusterableMapCell(gameMap.at(Position(0, 1))!!))

            expectedClusters[1].addPoint(ClusterableMapCell(gameMap.at(Position(20, 9))!!))
            expectedClusters[1].addPoint(ClusterableMapCell(gameMap.at(Position(19, 10))!!))
            expectedClusters[1].addPoint(ClusterableMapCell(gameMap.at(Position(20, 10))!!))
            expectedClusters[1].addPoint(ClusterableMapCell(gameMap.at(Position(21, 10))!!))
            expectedClusters[1].addPoint(ClusterableMapCell(gameMap.at(Position(20, 11))!!))

            expectedClusters[2].addPoint(ClusterableMapCell(gameMap.at(Position(35, 34))!!))
            expectedClusters[2].addPoint(ClusterableMapCell(gameMap.at(Position(34, 35))!!))
            expectedClusters[2].addPoint(ClusterableMapCell(gameMap.at(Position(35, 35))!!))
            expectedClusters[2].addPoint(ClusterableMapCell(gameMap.at(Position(36, 35))!!))
            expectedClusters[2].addPoint(ClusterableMapCell(gameMap.at(Position(35, 36))!!))

            val expectedBoundedClusters = expectedClusters.map{BoundedCluster(it)}

            val mapClusters = MapClusterer(gameMap,
                            gameMap.totalHalite / (gameMap.width * gameMap.height)).clusters

            assertEquals(expectedClusters.size, mapClusters.size)
            assertTrue(
                    expectedBoundedClusters.all { expectedBoundedCluster ->
                        mapClusters.any { mapCluster ->
                            mapCluster.cluster.points.containsAll(expectedBoundedCluster.cluster.points)
                                && mapCluster.minX == expectedBoundedCluster.minX
                                && mapCluster.maxX == expectedBoundedCluster.maxX
                                && mapCluster.midX == expectedBoundedCluster.midX
                                && mapCluster.minY == expectedBoundedCluster.minY
                                && mapCluster.maxY == expectedBoundedCluster.maxY
                                && mapCluster.midY == expectedBoundedCluster.midY
                        }
                    })
        }
    }

})
