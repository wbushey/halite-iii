package testHelpers

import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.io.PrintStream

/**
 * Write strings to a proxy Standard Input. Very useful for issuing commands and
 * instantiating Halite objects that read from Standard Input.
 *
 * Example:
 *
 *     val readString = withProxyStandardIn{
 *          it.println("some content")
 *          kotlin.io.readLine()
 *     }
 *     assertEquals("some content", readString)
 *
 * @param block Block to be executed with a proxy to standard in
 */
fun <T> withProxyStandardIn(block: (PrintStream) -> T): T {
    // Set Standard In to listen to our proxy
    val originalIn = System.`in`
    val proxy = PipedInputStream()
    System.setIn(proxy)

    // Execute the block and provide an interface for our proxy in
    val result = block(PrintStream(PipedOutputStream(proxy)))

    // Reset Standard In
    System.setIn(originalIn)

    // Return results
    return result
}