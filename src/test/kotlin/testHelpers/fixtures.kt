package testHelpers

import bot.assets.FleetStatus
import hlt.*

fun fleetStatusFixture(playerId: PlayerId = PlayerId(1),
                       shipYardPosition: Position = Position(63, 0)
) =
        FleetStatus(GameMap(Constants.MAP_WIDTH, Constants.MAP_HEIGHT), Shipyard(playerId, shipYardPosition),
                mutableMapOf(), mutableMapOf())

/**
 * Set game Constants to expected values based on game documentation
 */
fun resetConstants() {
    Constants.apply {
        MAX_HALITE = 1000
        SHIP_COST = 1000
        DROPOFF_COST = 4000
        MAX_TURNS = 500
        EXTRACT_RATIO = 4
        MOVE_COST_RATIO = 10
        INSPIRATION_ENABLED = true
        INSPIRATION_RADIUS = 4
        INSPIRATION_SHIP_COUNT = 2
        INSPIRED_EXTRACT_RATIO = EXTRACT_RATIO
        INSPIRED_BONUS_MULTIPLIER = 2.0
        INSPIRED_MOVE_COST_RATIO = MOVE_COST_RATIO
        MAP_WIDTH = 64
        MAP_HEIGHT = 64
    }
}