package hlt

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.assertEquals

object CommandSpec: Spek({
    describe("Spawn"){
        it("prints as 'g'"){
            assertEquals("g", Command.spawnShip().toString())
        }
    }

    describe("TransformToDropoff"){
        it("prints as 'c ID'"){
            assertEquals("c 10", Command.transformShipIntoDropoffSite(EntityId(10)).toString())
        }
    }

    describe("Move"){
        it("prints as 'm ID DIRECTION'"){
            assertEquals("m 10 n", Command.move(EntityId(10), Direction.NORTH).toString())
        }
    }

})
