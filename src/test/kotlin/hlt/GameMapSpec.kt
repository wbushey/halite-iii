package hlt

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.*

// Test helper to create ships and register them on the map
fun GameMap.createShip(player: Player, entityId: EntityId, position: Position) {
    player.ships[entityId] = Ship(player.id, entityId, position, 50)
    this.at(position)!!.markOccupiedBy(player.ships[entityId]!!)
}

object GameMapSpec : Spek({
    val gameMap = GameMap(64, 64)
    val players = listOf(
            Player(PlayerId(0), Shipyard(PlayerId(0), Position(15, 15))),
            Player(PlayerId(1), Shipyard(PlayerId(1), Position(47, 15))),
            Player(PlayerId(2), Shipyard(PlayerId(2), Position(15, 47))),
            Player(PlayerId(3), Shipyard(PlayerId(3), Position(47, 47)))
    )
    val currentPlayerId = PlayerId(0)

    beforeEachTest {
        players.forEach {
            it.ships.clear()
            it.dropoffs.clear()
        }

        // Give each player a ship
        gameMap.createShip(players[0], EntityId(1), Position(10, 10))
        gameMap.createShip(players[1], EntityId(1001), Position(20, 10))
        gameMap.createShip(players[2], EntityId(2001), Position(35, 22))
        // Player 4 had 3 ships
        gameMap.createShip(players[3], EntityId(4001), Position(43, 40))
        gameMap.createShip(players[3], EntityId(4002), Position(39, 37))
        gameMap.createShip(players[3], EntityId(4003), Position(50, 50))

        // give each player a dropoff
        players[0].dropoffs[EntityId(1101)] = DropOff(PlayerId(0), EntityId(1101), Position(17, 17))
        players[1].dropoffs[EntityId(2101)] = DropOff(PlayerId(1), EntityId(2101), Position(49, 17))
        players[2].dropoffs[EntityId(3101)] = DropOff(PlayerId(2), EntityId(3101), Position(17, 49))
        // Player 4 has 3 dropoffs
        players[3].dropoffs[EntityId(4101)] = DropOff(PlayerId(3), EntityId(4101), Position(49, 49))
        players[3].dropoffs[EntityId(4102)] = DropOff(PlayerId(3), EntityId(4102), Position(50, 50))
        players[3].dropoffs[EntityId(4103)] = DropOff(PlayerId(3), EntityId(4103), Position(50, 50))
    }

    context("setMapAtTurnStart") {
        beforeEachTest {
            gameMap.clearMarks()
            gameMap.setMapAtTurnStart(players, currentPlayerId)
        }
        it("marks positions with ships as occupied") {
            players.forEach { player ->
                player.ships.values.forEach { ship ->
                    assertTrue(gameMap.at(ship.position)!!.isOccupied)
                }
            }
        }

        it("marks positions of and around enemy ships as willBeOccupied") {
            players.forEach { player ->
                player.ships.values.forEach { ship ->
                    if (player.id != currentPlayerId) {
                        assertTrue(gameMap.at(ship.position)!!.willBeOccupied)
                        assertTrue(gameMap.at(ship.position.directionalOffset(Direction.EAST))!!.willBeOccupied)
                        assertTrue(gameMap.at(ship.position.directionalOffset(Direction.WEST))!!.willBeOccupied)
                        assertTrue(gameMap.at(ship.position.directionalOffset(Direction.NORTH))!!.willBeOccupied)
                        assertTrue(gameMap.at(ship.position.directionalOffset(Direction.SOUTH))!!.willBeOccupied)
                    } else {
                        assertFalse(gameMap.at(ship.position)!!.willBeOccupied)
                        assertFalse(gameMap.at(ship.position.directionalOffset(Direction.EAST))!!.willBeOccupied)
                        assertFalse(gameMap.at(ship.position.directionalOffset(Direction.WEST))!!.willBeOccupied)
                        assertFalse(gameMap.at(ship.position.directionalOffset(Direction.NORTH))!!.willBeOccupied)
                        assertFalse(gameMap.at(ship.position.directionalOffset(Direction.SOUTH))!!.willBeOccupied)
                    }
                }
            }
        }

        it("sets the correct number of nearByEnemyShips around enemy ships") {
            players.forEach { player ->
                if (player.id != currentPlayerId) {
                    player.ships.values.forEach { ship ->
                        gameMap.breadthFirstTraversal(ship.position, Constants.INSPIRATION_RADIUS) {
                            // Several cells are near by more than one of Player 4's ships
                            if (listOf(Position(39, 40),
                                            Position(40, 39), Position(40, 40),
                                            Position(41, 38), Position(41, 39),
                                            Position(42, 37), Position(42, 38),
                                            Position(43, 37)).contains(it.position)) {
                                assertEquals(2, it.nearByEnemyShips)
                            } else {
                                assertEquals(1, it.nearByEnemyShips)
                            }
                        }
                    }
                }
            }

        }

        it("marks positions of shipyards and dropoffs as structures") {
            gameMap.setMapAtTurnStart(players, currentPlayerId)

            players.forEach { player ->
                assertTrue(gameMap.at(player.shipyard.position)!!.hasStructure)
                player.dropoffs.values.forEach { dropoff ->
                    assertTrue(gameMap.at(dropoff.position)!!.hasStructure)
                }
            }
        }

    }

    context("clearMarks") {
        beforeEachTest {
            gameMap.setMapAtTurnStart(players, currentPlayerId)
            gameMap.clearMarks()
        }
        it("removes all current and future ship marks from the map") {
            (0..gameMap.width).forEach { x ->
                (0..gameMap.height).forEach { y ->
                    assertFalse(gameMap.at(Position(x, y))!!.isOccupied)
                    assertFalse(gameMap.at(Position(x, y))!!.willBeOccupied)
                }
            }
        }

        it("sets nearByEnemyShips to zero for every cell") {
            (0..gameMap.width).forEach { x ->
                (0..gameMap.height).forEach { y ->
                    assertEquals(0, gameMap.at(Position(x, y))!!.nearByEnemyShips)
                }
            }
        }

    }

    context("subSet") {
        it("provides a Set containing the specified cells") {
            // Expecting to get all the cells between 10, 10 and 12,12, inclusive
            val expectedCells = setOf(
                    *gameMap.cells.filterIndexed { y, _ -> y in 10..12 }.flatMap { row ->
                        row.filterIndexed { x, _ -> x in 10..12 }
                    }.toTypedArray()
            )

            assertEquals(expectedCells, gameMap.subSet((10..12), (10..12)))
        }

        it("handles toroidal coordinates") {
            // Expected cells that straddle the boundaries of 2D space
            val expectedCells = setOf(
                    *gameMap.cells.filterIndexed { y, _ -> y in 0..1 || y == 63 }.flatMap { row ->
                        row.filterIndexed { x, _ -> x in 0..1 || x == 63 }
                    }.toTypedArray()
            )

            assertEquals(expectedCells, gameMap.subSet((-1..1), (-1..1)))
            assertEquals(expectedCells, gameMap.subSet((63..65), (63..65)))
            assertEquals(expectedCells, gameMap.subSet((-1..1), (63..65)))
            assertEquals(expectedCells, gameMap.subSet((63..65), (-1..1)))
        }
    }
})
