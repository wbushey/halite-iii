package hlt

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue

object ShipSpec : Spek({
    val ship = Ship(PlayerId(1), EntityId(11), Position(1, 1), 1000)
    context("moveTo") {
        val map = GameMap(10, 10)
        it("returns a Move Command for the ship in the correct direction") {
            Direction.ALL_DIRECTIONS.forEach {
                val command = ship.moveTo(it, map)
                assertEquals(it, command.direction)
                assertEquals(ship.id, command.shipId)
            }
        }

        it("marks the Position being moved into as willBeOccupied"){
            map.clearMarks()
            Direction.ALL_DIRECTIONS.forEach {
                assertTrue(map.at(ship.position.directionalOffset(it))!!.willNotBeOccupied)
                ship.moveTo(it, map)
                assertTrue(map.at(ship.position.directionalOffset(it))!!.willBeOccupied)
            }
        }
    }

    context("stayStill"){
        val map = GameMap(10, 10)
        it("returns a Move Command for the ship to stay still") {
            val command = ship.stayStill(map)
            assertEquals(Direction.STILL, command.direction)
            assertEquals(ship.id, command.shipId)
        }

        it("marks the Position being moved into as willBeOccupied"){
            map.clearMarks()
            assertTrue(map.at(ship.position)!!.willNotBeOccupied)
            ship.stayStill(map)
            assertTrue(map.at(ship.position)!!.willBeOccupied)
        }

    }
})

