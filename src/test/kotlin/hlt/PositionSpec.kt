package hlt

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions.assertEquals

object PositionSpec: Spek({
    it("is always normalized"){
        // Default test map width and height are 64
        val inBoundPosition = Position(10, 20)
        assertEquals(10, inBoundPosition.x)
        assertEquals(20, inBoundPosition.y)

        val xUnderPosition = Position(-10, 20)
        assertEquals(54, xUnderPosition.x)
        assertEquals(20, xUnderPosition.y)

        val yUnderPosition = Position(10, -20)
        assertEquals(10, yUnderPosition.x)
        assertEquals(44, yUnderPosition.y)

        val xyUnderPosition = Position(-10, -20)
        assertEquals(54, xyUnderPosition.x)
        assertEquals(44, xyUnderPosition.y)

        val xOverPosition = Position(74, 20)
        assertEquals(10, xOverPosition.x)
        assertEquals(20, xOverPosition.y)

        val yOverPosition = Position(10, 84)
        assertEquals(10, yOverPosition.x)
        assertEquals(20, yOverPosition.y)

        val xyOverPosition = Position(74, 84)
        assertEquals(10, xyOverPosition.x)
        assertEquals(20, xyOverPosition.y)

        val xyOnEdge = Position(64, 64)
        assertEquals(0, xyOnEdge.x)
        assertEquals(0, xyOnEdge.y)
    }
})
