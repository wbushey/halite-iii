#!/bin/bash

REPLAYS_AND_FLOG_DIR="${REPLAYS_DIR:-replays}"
filename="$(ls -rt $REPLAYS_AND_FLOG_DIR | grep replay | tail -n 1).flog"
filePath="$REPLAYS_AND_FLOG_DIR/$filename"
echo "[" > ${filePath}
cat /tmp/flogs/* >> ${filePath}
rm /tmp/flogs/*
