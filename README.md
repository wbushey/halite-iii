# Halite III Bot

## Setup

1. Install the version of Java specified in `.java-version`
2. Copy the `halite` executable to somewhere on your PATH. [Download it from here](https://halite.io/learn-programming-challenge/downloads).

## Run in local game

### Simple Run

`./run_game.sh`

### Gym

* Register a bot: `hlt gym register NAME "java -Dlog.level=debug -jar PATH_TO_JAR"`
* Run the gym: `./gym_workout.sh`  --OR-- `hlt gym evaluate -b halite -i 100 --output-dir ../replays/gym`
* See Results: `hlt gym bots`

## Logging

Bot's default log level is `error`. This can be changed at runtime by setting the `log.level` system property, e.g. `java -Dlog.level=debug -jar build/libs/MyBot.jar`. Run executed by `./run_game.sh`, the log level will be `debug`.

### Saving F-Logs

F-Logs will be created for each bot in `/tmp/flogs/` if the log level is INFO or less specific

`run_game.sh` will save an flog along with the game's replay. If you run a game some other way, you can run
`./save_flog.sh` to save the f-log of the most recent game in the 'replays' folder.

The 'replays' folder can be specified by defining the `REPLAYS_DIR` environment variable.

## CLI

The Halite executable comes with a command line interface (CLI). Run `$ ./halite --help` to see a full listing of available flags.

## Submitting the bot

* `./gradlew zip` to create a zip with a jar that can be executed by the Halite servers
* Submit zipped file in `build/distributions` to: https://halite.io/play-programming-challenge

## Custom compilation on our game servers
* Your bot has `10 minutes` to install dependencies and compile on the game server.
* You can run custom commands to set up your bot by including an `install.sh` file alongside `MyBot.{ext}`. This file will be executed and should be a Bash shell script. You must include the shebang line at the top: `#!/bin/bash`.
  * For Python, you may install packages using pip, but you may not install to the global package directory. Instead, install packages as follows: `python3.6 -m pip install --system --target . numpy`
* Some languages don't use the `MyBot.{ext}` convention. Exceptions include:
  * Rust: a Cargo.toml in the root will be detected as Rust. Your bot will compile with `cargo rustc`.
  * Swift: a Package.swift in the root will be detected as Swift. Your bot will compile with `swift build`.
  * Haskell: You may upload a MyBot.hs, or you may upload a `stack.yaml`, in which case your bot will compile with `stack build`.
  * Elixir: Upload a mix.exs. Your bot will compile with `mix deps.get` followed by `mix escript.build`.
  * Clojure: Upload a project.clj. Your bot will compile with `lein uberjar`.
  * .NET: Upload a MyBot.csproj or MyBot.fsproj. Your bot will compile with `dotnet restore` followed with `dotnet build`.

## Acknowledgements

This Kotlin Halite starter repo builds upon the [Kotlin starter kit provided by Two Sigma](https://halite.io/learn-programming-challenge/downloads).
